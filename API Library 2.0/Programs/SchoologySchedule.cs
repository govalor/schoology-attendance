﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    
    public class SchoologySchedule
    {
        //table specific
        public static string URL_BASE_STUDENTS = "https://master-api1.nextworld.net/v2/JVGetStudentsResult?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_TEACHERS = "https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_S_SCHED = "https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_T_SCHED = "https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:";
        public static string URL_BASE_S_SCHED_ALL = "https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_BASE_T_SCHED_ALL = "https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwPaging=%7B%22limit%22:15,%22offset%22:";

        //generic
        public static string URL_P2 = "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:";
        public static string URL_P3 = "%7D%7D%5D%7D&nwPaging=%7B%22limit%22:15,%22offset%22:";
        public static string URL_END = "%7D";

        //used for token authentication
        private static TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private static string ACCESS_TOKEN = "";
        private static string ZONE = "AppStable";
        private static string LIFECYCLE = "jake_v";

        public static List<GetStudents_Result> GetStudents()
        {
            List<GetStudents_Result> temp = new List<GetStudents_Result>();

            StudentNW.RootObject allStudentsFromNW = new StudentNW.RootObject();

            //allStudentsFromNW = getAllStudentsRootObjectAPI("https://master-api1.nextworld.net/v2/JVGetStudentsResult");
            allStudentsFromNW = getAllStudentsLoop(); //api paging

            temp = convertAllStudentsToOldModle(allStudentsFromNW);
            return temp;
        }

        private static List<GetStudents_Result> convertAllStudentsToOldModle(StudentNW.RootObject allStudentsFromNW)
        {
            List<GetStudents_Result> temp = new List<GetStudents_Result>();

            List<StudentNW.Record> records = new List<StudentNW.Record>();
            records = allStudentsFromNW.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(convertStudentToOldModle(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetStudents_Result convertStudentToOldModle(StudentNW.AppData appData)
        {
            GetStudents_Result temp = new GetStudents_Result();

            temp.ClassOf = appData.JVClassOf;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.Title = appData.JVTitle;
            temp.UserDefinedID = appData.JVUserDefinedID;

            return temp;
        }

        private static StudentNW.RootObject getAllStudentsRootObjectAPI(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        private static StudentNW.RootObject getAllStudentsLoop()
        {
            StudentNW.RootObject root = new StudentNW.RootObject();
            List<StudentNW.Record> data = new List<StudentNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_STUDENTS + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_STUDENTS + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new StudentNW.RootObject();
                root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new StudentNW.RootObject();
            StudentNW.Data rootData = new StudentNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        //public virtual ObjectResult<GetStudentSchedule_Result> GetStudentSchedule(Nullable<int> eA7RecordsId)
        //{
        //    var eA7RecordsIdParameter = eA7RecordsId.HasValue ?
        //        new ObjectParameter("EA7RecordsId", eA7RecordsId) :
        //        new ObjectParameter("EA7RecordsId", typeof(int));

        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetStudentSchedule_Result>("GetStudentSchedule", eA7RecordsIdParameter);
        //}

        public static List<GetStudentSchedule_Result> GetStudentSchedule(Nullable<int> eA7RecordsId)
        {
            List<GetStudentSchedule_Result> temp = new List<GetStudentSchedule_Result>();
            //CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId);
            CoursesNWGET.RootObject root = generateStudentScheduleLoop(eA7RecordsId); //api paging

            List<CoursesNWGET.AppData> appDatas = new List<CoursesNWGET.AppData>();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                appDatas.Add(root.Data.records.ElementAt(i).appData);
            }

            GetStudentSchedule_Result tempSchedule = new GetStudentSchedule_Result();

            foreach (CoursesNWGET.AppData appData in appDatas)
            {
                tempSchedule = new GetStudentSchedule_Result();
                tempSchedule.BlockColor = appData.JVBlockColor;
                tempSchedule.BlockColorRGB = appData.JVBlockColorRGB;
                tempSchedule.ClassName = appData.JVClassName;
                tempSchedule.Course = appData.JVCourse;
                tempSchedule.CourseSection = appData.JVCourseSection;
                tempSchedule.CycleDaysID = appData.JVCycleDaysID;
                tempSchedule.Day = appData.JVDay;
                tempSchedule.EndTime = appData.JVEndTime;
                tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVEndTimeRaw, 0);
                tempSchedule.Faculty = appData.JVFaculty;
                tempSchedule.Room = appData.JVRoom;
                tempSchedule.Semester = appData.JVSemester;
                tempSchedule.StartTime = appData.JVStartTime;
                // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVStartTimeRaw, 0);
                temp.Add(tempSchedule);
            }


            return temp;
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        private static CoursesNWGET.RootObject generateStudentScheduleLoop(Nullable<int> ea7)
        {
            List<CoursesNWGET.Record> data = new List<CoursesNWGET.Record>();
            CoursesNWGET.RootObject root = new CoursesNWGET.RootObject();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_S_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new CoursesNWGET.RootObject();
                root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new CoursesNWGET.RootObject();
            CoursesNWGET.Data rootData = new CoursesNWGET.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        private static CoursesNWGET.RootObject generateStudentSchedule(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        private static CoursesNWGET.RootObject generateStudentSchedAll()
        {
            List<CoursesNWGET.Record> data = new List<CoursesNWGET.Record>();
            CoursesNWGET.RootObject root = new CoursesNWGET.RootObject();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_S_SCHED_ALL + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_S_SCHED_ALL + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new CoursesNWGET.RootObject();
                root = JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new CoursesNWGET.RootObject();
            CoursesNWGET.Data rootData = new CoursesNWGET.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        // Old method that uses blackbaud and SQL
        //public virtual ObjectResult<GetTeachers_Result> GetTeachers()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeachers_Result>("GetTeachers");
        //}

        public static List<GetTeachers_Result> GetTeachers()
        {
            List<GetTeachers_Result> temp = new List<GetTeachers_Result>();

            //TeacherNW.RootObject root = getAllTeacherRootObject("https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult/");
            TeacherNW.RootObject root = getAllTeacherRootLoop(); //api paging

            temp = convertRootObjectTeacherIntoModel(root);

            return temp;
        }

        private static List<GetTeachers_Result> convertRootObjectTeacherIntoModel(TeacherNW.RootObject root)
        {
            List<GetTeachers_Result> temp = new List<GetTeachers_Result>();

            List<TeacherNW.Record> records = new List<TeacherNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetTeachers_Result covertTeacherRecordtoOldModel(TeacherNW.AppData appData)
        {
            GetTeachers_Result temp = new GetTeachers_Result();

            temp.Department = appData.JVDepartment;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.EmailAddress;
            temp.FacultyID = appData.JVFacultyID;
            temp.FirstName = appData.JVFirstName;
            temp.LastName = appData.JVLastName;
            temp.SchoolID = appData.JVSchoolIDForFacutly;

            return temp;
        }

        private static TeacherNW.RootObject getAllTeacherRootObject(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            TeacherNW.RootObject root = new TeacherNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        private static TeacherNW.RootObject getAllTeacherRootLoop()
        {
            TeacherNW.RootObject root = new TeacherNW.RootObject();
            List<TeacherNW.Record> data = new List<TeacherNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_TEACHERS + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_TEACHERS + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new TeacherNW.RootObject();
                root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new TeacherNW.RootObject();
            TeacherNW.Data rootData = new TeacherNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        // Old one with SQL
        //public virtual ObjectResult<GetTeacherSchedule_Result> GetTeacherSchedule(Nullable<int> eA7RecordsId)
        //{
        //    var eA7RecordsIdParameter = eA7RecordsId.HasValue ?
        //        new ObjectParameter("EA7RecordsId", eA7RecordsId) :
        //        new ObjectParameter("EA7RecordsId", typeof(int));

        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetTeacherSchedule_Result>("GetTeacherSchedule", eA7RecordsIdParameter);
        //}

        // Jake Vossen read NW applicaiton
        public static List<GetTeacherSchedule_Result> GetTeacherSchedule(Nullable<int> eA7RecordsId)
        {
            List<GetTeacherSchedule_Result> temp = new List<GetTeacherSchedule_Result>();

            //TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId);
            TeacherScheduleNW.RootObject root = generateTeacherSchedLoop(eA7RecordsId); //api looping

            temp = convertRootObjectTeacherScheduleIntoModel(root);

            return temp;
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedLoop(Nullable<int> ea7)
        {
            TeacherScheduleNW.RootObject root = new TeacherScheduleNW.RootObject();
            List<TeacherScheduleNW.Record> data = new List<TeacherScheduleNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_T_SCHED + ea7.ToString() + URL_P2 + ea7.ToString() + URL_P3 + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new TeacherScheduleNW.RootObject();
                root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new TeacherScheduleNW.RootObject();
            TeacherScheduleNW.Data rootData = new TeacherScheduleNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedAll()
        {
            TeacherScheduleNW.RootObject root = new TeacherScheduleNW.RootObject();
            List<TeacherScheduleNW.Record> data = new List<TeacherScheduleNW.Record>();

            //initial request to get data from Nextworld
            RestClient client = new RestClient(URL_BASE_T_SCHED_ALL + "0" + URL_END);
            RestRequest request = new RestRequest(Method.GET);

            //using a token bearer to authenticate request
            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            IRestResponse response = client.Execute(request);

            root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);

            data = root.Data.records;

            //Loop through the student api and concatenate all the schedules for later use
            int endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < root.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_T_SCHED_ALL + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                //using a token bearer to authenticate request
                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                root = new TeacherScheduleNW.RootObject();
                root = JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response1.Content);

                data = data.Concat(root.Data.records).ToList();

                //update the end test
                endTest = root.Data.pageData.offset + root.Data.pageData.limit;
            }

            root = new TeacherScheduleNW.RootObject();
            TeacherScheduleNW.Data rootData = new TeacherScheduleNW.Data();
            rootData.records = data;
            root.Data = rootData;

            return root;
        }

        private static List<GetTeacherSchedule_Result> convertRootObjectTeacherScheduleIntoModel(TeacherScheduleNW.RootObject root)
        {
            List<GetTeacherSchedule_Result> temp = new List<GetTeacherSchedule_Result>();

            List<TeacherScheduleNW.Record> records = new List<TeacherScheduleNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherScheduleRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static GetTeacherSchedule_Result covertTeacherScheduleRecordtoOldModel(TeacherScheduleNW.AppData appData)
        {
            GetTeacherSchedule_Result temp = new GetTeacherSchedule_Result();

            temp.BlockColor = appData.JVBlockColor;
            temp.BlockColorRGB = appData.JVBlockColorRGB;
            temp.ClassName = appData.JVClassName;
            temp.Course = appData.JVCourse;
            temp.CourseSection = appData.JVCourseSection;
            temp.CycleDaysID = appData.JVCycleDaysID;
            temp.Day = appData.JVDay;
            temp.EndTime = appData.JVEndTime;
            //temp.EndTimeRaw = new System.DateTime(1, 1, 1, 0,appData.JVEndTimeRaw, 0);
            temp.StartTime = appData.JVStartTime;
            //temp.StartTimeRaw = new System.DateTime(1, 1, 1, 0, appData.JVEndTimeRaw, 0);

            return temp;
        }

        // Old Method With SQL
        //public virtual ObjectResult<GetAllStudentsSchedule_Result> GetAllStudentsSchedule()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllStudentsSchedule_Result>("GetAllStudentsSchedule");
        //}

        public static List<GetAllStudentsSchedule_Result> GetAllStudentsSchedule()
        {
            List<GetAllStudentsSchedule_Result> temp = new List<GetAllStudentsSchedule_Result>();

            // CoursesNWGET.RootObject root = generateStudentSchedule("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult");
            CoursesNWGET.RootObject root = generateStudentSchedAll(); //api paging

             GetAllStudentsSchedule_Result tempStudentsAllSchedule = new GetAllStudentsSchedule_Result();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                tempStudentsAllSchedule = new GetAllStudentsSchedule_Result();
                tempStudentsAllSchedule.Course = root.Data.records.ElementAt(i).appData.JVCourse;
                tempStudentsAllSchedule.BlockColor = root.Data.records.ElementAt(i).appData.JVBlockColor;
                tempStudentsAllSchedule.BlockColorRGB = root.Data.records.ElementAt(i).appData.JVBlockColorRGB;
                tempStudentsAllSchedule.ClassName = root.Data.records.ElementAt(i).appData.JVClassName;
                tempStudentsAllSchedule.CourseSection = root.Data.records.ElementAt(i).appData.JVCourseSection;
                tempStudentsAllSchedule.CycleDaysID = root.Data.records.ElementAt(i).appData.JVCycleDaysID;
                tempStudentsAllSchedule.UserUniqueID = root.Data.records.ElementAt(i).appData.JVEA7RecordsID;
                tempStudentsAllSchedule.Day = root.Data.records.ElementAt(i).appData.JVDay;
                tempStudentsAllSchedule.EndTime = root.Data.records.ElementAt(i).appData.JVEndTime;
                tempStudentsAllSchedule.Faculty = root.Data.records.ElementAt(i).appData.JVFaculty;
                tempStudentsAllSchedule.Room = root.Data.records.ElementAt(i).appData.JVRoom;
                tempStudentsAllSchedule.Semester = root.Data.records.ElementAt(i).appData.JVSemester;
                tempStudentsAllSchedule.StartTime = root.Data.records.ElementAt(i).appData.JVStartTime;
                temp.Add(tempStudentsAllSchedule);
            }

            return temp;
        }

        // Old method with SQL
        //public virtual ObjectResult<GetAllTeachersSchedule_Result> GetAllTeachersSchedule()
        //{
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllTeachersSchedule_Result>("GetAllTeachersSchedule");
        //}

        public static List<GetAllTeachersSchedule_Result> GetAllTeachersSchedule()
        {
            List<GetAllTeachersSchedule_Result> temp = new List<GetAllTeachersSchedule_Result>();

            //TeacherScheduleNW.RootObject root = generateTeacherSchedule("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult");
            TeacherScheduleNW.RootObject root = generateTeacherSchedAll(); //api paging

            GetAllTeachersSchedule_Result tempTeachersAllSchedule = new GetAllTeachersSchedule_Result();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                tempTeachersAllSchedule = new GetAllTeachersSchedule_Result();
                tempTeachersAllSchedule.Course = root.Data.records.ElementAt(i).appData.JVCourse;
                tempTeachersAllSchedule.BlockColor = root.Data.records.ElementAt(i).appData.JVBlockColor;
                tempTeachersAllSchedule.BlockColorRGB = root.Data.records.ElementAt(i).appData.JVBlockColorRGB;
                tempTeachersAllSchedule.ClassName = root.Data.records.ElementAt(i).appData.JVClassName;
                tempTeachersAllSchedule.CourseSection = root.Data.records.ElementAt(i).appData.JVCourseSection;
                tempTeachersAllSchedule.CycleDaysID = root.Data.records.ElementAt(i).appData.JVCycleDaysID;
                tempTeachersAllSchedule.Day = root.Data.records.ElementAt(i).appData.JVDay;
                tempTeachersAllSchedule.EA7RecordsID = root.Data.records.ElementAt(i).appData.JVEA7RecordsID;
                tempTeachersAllSchedule.EndTime = root.Data.records.ElementAt(i).appData.JVEndTime;
                tempTeachersAllSchedule.Faculty = root.Data.records.ElementAt(i).appData.JVFaculty;
                tempTeachersAllSchedule.Room = root.Data.records.ElementAt(i).appData.JVRoom;
                tempTeachersAllSchedule.Semester = root.Data.records.ElementAt(i).appData.JVSemester;
                tempTeachersAllSchedule.StartTime = root.Data.records.ElementAt(i).appData.JVStartTime;
                temp.Add(tempTeachersAllSchedule);
            }

            return temp;
        }

        public static List<GetStudentScheduleBySemester_Result> GetStudentScheduleBySemester(Nullable<int> eA7RecordsId, Nullable<int> semester)
        {
            // fall = 1
            // spring = 2
            List<GetStudentScheduleBySemester_Result> temp = new List<GetStudentScheduleBySemester_Result>();

            List<GetStudentSchedule_Result> allClasses = GetStudentSchedule(eA7RecordsId);
            GetStudentScheduleBySemester_Result tempSchedule = new GetStudentScheduleBySemester_Result();
            for (int i = 0; i < allClasses.Count; i++)
            {
                if (semester == 1)
                {
                    if (allClasses.ElementAt(i).Semester.ToLower().Equals("fall"))
                    {
                        tempSchedule = new GetStudentScheduleBySemester_Result();
                        tempSchedule.BlockColor = allClasses.ElementAt(i).BlockColor;
                        tempSchedule.BlockColorRGB = allClasses.ElementAt(i).BlockColorRGB;
                        tempSchedule.ClassName = allClasses.ElementAt(i).ClassName;
                        tempSchedule.Course = allClasses.ElementAt(i).Course;
                        tempSchedule.CourseSection = allClasses.ElementAt(i).CourseSection;
                        tempSchedule.CycleDaysID = allClasses.ElementAt(i).CycleDaysID;
                        tempSchedule.Day = allClasses.ElementAt(i).Day;
                        tempSchedule.EndTime = allClasses.ElementAt(i).EndTime;
                        //tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVEndTimeRaw, 0);
                        tempSchedule.Faculty = allClasses.ElementAt(i).Faculty;
                        tempSchedule.Room = allClasses.ElementAt(i).Room;
                        tempSchedule.Semester = allClasses.ElementAt(i).Semester;
                        tempSchedule.StartTime = allClasses.ElementAt(i).StartTime;
                        // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVStartTimeRaw, 0);
                        temp.Add(tempSchedule);
                    }
                }
                else
                {
                    if (allClasses.ElementAt(i).Semester.ToLower().Equals("spring"))
                    {
                        tempSchedule = new GetStudentScheduleBySemester_Result();
                        tempSchedule.BlockColor = allClasses.ElementAt(i).BlockColor;
                        tempSchedule.BlockColorRGB = allClasses.ElementAt(i).BlockColorRGB;
                        tempSchedule.ClassName = allClasses.ElementAt(i).ClassName;
                        tempSchedule.Course = allClasses.ElementAt(i).Course;
                        tempSchedule.CourseSection = allClasses.ElementAt(i).CourseSection;
                        tempSchedule.CycleDaysID = allClasses.ElementAt(i).CycleDaysID;
                        tempSchedule.Day = allClasses.ElementAt(i).Day;
                        tempSchedule.EndTime = allClasses.ElementAt(i).EndTime;
                        //tempSchedule.EndTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVEndTimeRaw, 0);
                        tempSchedule.Faculty = allClasses.ElementAt(i).Faculty;
                        tempSchedule.Room = allClasses.ElementAt(i).Room;
                        tempSchedule.Semester = allClasses.ElementAt(i).Semester;
                        tempSchedule.StartTime = allClasses.ElementAt(i).StartTime;
                        // tempSchedule.StartTimeRaw = new System.DateTime(1, 1, 1, 0, allClasses.ElementAt(i)JVStartTimeRaw, 0);
                        temp.Add(tempSchedule);
                    }
                }
            }

            return temp;

        }
    }
}
