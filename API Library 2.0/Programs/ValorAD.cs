﻿using API_Library_2._0.Final_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class ValorAD
    {
        //for api looping
        private string URL_BASE_FACULTY = "https://master-api1.nextworld.net/v2/CMFacultyAD?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_BASE_STUDENT = "https://master-api1.nextworld.net/v2/CMActiveDirectory?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string URL_END = "%7D";

        //used for token authentication
        private TokenAuthentication tokenCreator = new TokenAuthentication("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string ACCESS_TOKEN = "";
        private string ZONE = "AppStable";
        private string LIFECYCLE = "cheyanne";

        public ValorAD(){}

        public List<StudentModel> getADStudents()
        {
            List<StudentModel> list = new List<StudentModel>();

            //Execute request to retrieve data from Nextworld
            StudentGetModel.RootObject getRoot = new StudentGetModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMActiveDirectory");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //to deserialize the response
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<StudentGetModel.RootObject>(response.Content);

            //translate into appropriate model
            list = toStudentModel(getRoot.Data.records);

            return list;
        }

        public List<StudentModel> getADStudentsLoop()
        {
            List<StudentModel> list = new List<StudentModel>();
            StudentGetModel.RootObject getRoot = new StudentGetModel.RootObject();

            //request to retrieve initial data from Nextworld
            var client = new RestClient(URL_BASE_STUDENT + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");
            
            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            getRoot = JsonConvert.DeserializeObject<StudentGetModel.RootObject>(response.Content);

            list = toStudentModel(getRoot.Data.records);

            //Loop through the student api and concatenate all the students for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_STUDENT + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new StudentGetModel.RootObject();
                getRoot = JsonConvert.DeserializeObject<StudentGetModel.RootObject>(response1.Content);

                list = list.Concat(toStudentModel(getRoot.Data.records)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }
            return list;
        }

        public List<StudentModel> toStudentModel(List<StudentGetModel.Record> list)
        {
            List<StudentModel> newList = new List<StudentModel>();
            StudentModel newStudent;
            StudentGetModel.AppData oldStudent;

            for (int i = 0; i < list.Count(); i++)
            {
                newStudent = new StudentModel();
                oldStudent = list.ElementAt(i).appData;

                newStudent.EA7RecordsID = oldStudent.CMEA7RecordsID;
                newStudent.StudentID = oldStudent.CMStudentIDNumber;
                newStudent.NickName = oldStudent.CMNickName;
                newStudent.Title = oldStudent.CMTitle;
                newStudent.FirstName = oldStudent.CMFirstName;
                newStudent.MiddleName = oldStudent.CMMiddleName;
                newStudent.LastName = oldStudent.CMLastName;
                newStudent.Gender = oldStudent.CMGender;
                newStudent.GradeLevel = oldStudent.CMGradeLevel;
                newStudent.OUDefaultGroup = oldStudent.CMOUDefaultGroup;
                newStudent.OUClassGroup = oldStudent.CMOUClassGroup;
                newStudent.OULocation = oldStudent.CMOULocation;
                newStudent.Email = oldStudent.CMEmail;
                newStudent.Password = oldStudent.CMPassword;

                newList.Add(newStudent);
            }

            return newList;
        }

        public List<FacultyStaffModel> getADFacultyStaff(String option)
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();

            //Execute Get request to retrieve data from Nextworld
            FacultyGetModel.RootObject getRoot = new FacultyGetModel.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyAD");

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //to deserialize the response
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<FacultyGetModel.RootObject>(response.Content);

            //translate into appropriate model
            list = toFacultyStaffModel(option, getRoot.Data.records);

            return list;
        }

        public List<FacultyStaffModel> getFSLoop(string option)
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();
            FacultyGetModel.RootObject getRoot = new FacultyGetModel.RootObject();

            //Get request to retrieve initial data from Nextworld
            var client = new RestClient(URL_BASE_FACULTY + "0" + URL_END);

            ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
            client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
            client.AddDefaultHeader("Content-Type", "application/json");

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            getRoot = JsonConvert.DeserializeObject<FacultyGetModel.RootObject>(response.Content);

            list = toFacultyStaffModel(option, getRoot.Data.records);

            //Loop through the student api and concatenate all the students for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = URL_BASE_FACULTY + nextOffset + URL_END;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);

                ACCESS_TOKEN = tokenCreator.getToken(ZONE, LIFECYCLE);
                client.AddDefaultHeader("Authorization", "Bearer " + ACCESS_TOKEN);
                client.AddDefaultHeader("Content-Type", "application/json");

                IRestResponse response1 = client.Execute(request);

                getRoot = new FacultyGetModel.RootObject();
                getRoot = JsonConvert.DeserializeObject<FacultyGetModel.RootObject>(response1.Content);

                list = list.Concat(toFacultyStaffModel(option, getRoot.Data.records)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }
            return list;
        }

        public List<FacultyStaffModel> toFacultyStaffModel(String type, List<FacultyGetModel.Record> toTranslate)
        {
            List<FacultyStaffModel> list = new List<FacultyStaffModel>();
            FacultyStaffModel newFaculty;
            FacultyGetModel.AppData oldFaculty;
            String testType = "";

            for (int i = 0; i < toTranslate.Count(); i++)
            {
                oldFaculty = toTranslate.ElementAt(i).appData;

                //filters to only retrieve the indicated type of AD record: Faculty or Staff
                testType = "Valor " + type;
                if (oldFaculty.CMOUDefaultGroup.Equals(testType))
                {
                    newFaculty = new FacultyStaffModel();

                    newFaculty.EA7RecordsID = oldFaculty.CMEA7RecordsID;
                    newFaculty.FacultyID = oldFaculty.CMFacultyID;
                    newFaculty.NickName = oldFaculty.CMNickName;
                    newFaculty.FirstName = oldFaculty.CMFirstName;
                    newFaculty.LastName = oldFaculty.CMLastName;
                    newFaculty.Gender = oldFaculty.CMGender;
                    newFaculty.OULocation = oldFaculty.CMOULocation;
                    newFaculty.OUDefaultGroup = oldFaculty.CMOUDefaultGroup;
                    newFaculty.OUClassGroup = oldFaculty.CMOUClassGroup;
                    newFaculty.Email = oldFaculty.CMEmail;
                    newFaculty.Password = oldFaculty.CMPassword;

                    list.Add(newFaculty);
                }
            }
            return list;
        }
    }
}
