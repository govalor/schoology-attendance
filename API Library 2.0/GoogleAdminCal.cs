﻿using API_Library_2._0.Final_Models;
using API_Library_2._0.Translation_Models;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class GoogleAdminCal
    {
        public static HttpBasicAuthenticator auth = new HttpBasicAuthenticator("jake.vossen@nextworld.net", "gmAHF7eS9!BI$20MzFkk0nW3qz&LdXykwUs!RSBq");
        public static List<StudentModel> GetStudents()
        {
            //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
            List<StudentModel> temp = new List<StudentModel>();


            StudentNW.RootObject allStudentsFromNW = new StudentNW.RootObject();

            allStudentsFromNW = getAllStudentsRootObjectAPI("https://master-api1.nextworld.net/v2/JVGetStudentsResult");

            for (int i = 0; i < allStudentsFromNW.Data.records.Count; i++)
            {
                temp.Add(convertAllStudentsToOldModle(allStudentsFromNW.Data.records.ElementAt(i).appData));

            }
            //logger.Debug("Converted the NW model to the old model");
            return temp;
        }

        private static StudentModel convertAllStudentsToOldModle(StudentNW.AppData appData)
        {
            StudentModel temp = new StudentModel();

            temp.ClassOf = appData.JVClassOf;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.JVEmail;
            temp.FirstName = appData.JVFirstName;
            temp.Gender = appData.JVGender;
            temp.GradeLevel = appData.JVGradeLevel;
            temp.LastName = appData.JVLastName;
            temp.MiddleName = appData.JVMiddleName;
            temp.NickName = appData.JVNickName;
            temp.Title = appData.JVTitle;
            temp.UserDefinedID = appData.JVUserDefinedID;

            return temp;
        }

        private static StudentNW.RootObject getAllStudentsRootObjectAPI(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            StudentNW.RootObject root = new StudentNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<StudentNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }


        // Old way with SQL
        //public virtual List<StudentScheduleModel> GetStudentSchedule(int id)
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
        //    return this.Database.SqlQuery<StudentScheduleModel>("[Schoology].[GetStudentSchedule] " + id).ToList();
        //}

        public static List<StudentScheduleModel> GetStudentSchedule(int eA7RecordsId)
        {
            List<StudentScheduleModel> temp = new List<StudentScheduleModel>();

            CoursesNWGET.RootObject root = generateStudentSchedule(eA7RecordsId);

            List<CoursesNWGET.AppData> appDatas = new List<CoursesNWGET.AppData>();

            for (int i = 0; i < root.Data.records.Count; i++)
            {
                appDatas.Add(root.Data.records.ElementAt(i).appData);
            }

            StudentScheduleModel tempSchedule = new StudentScheduleModel();

            foreach (CoursesNWGET.AppData appData in appDatas)
            {
                tempSchedule = new StudentScheduleModel();
                tempSchedule.BlockColor = appData.JVBlockColor;
                tempSchedule.BlockColorRGB = appData.JVBlockColorRGB;
                tempSchedule.ClassName = appData.JVClassName;
                tempSchedule.Course = appData.JVCourse;
                tempSchedule.CourseSection = appData.JVCourseSection;
                // tempSchedule.CycleDaysID = appData.JVCycleDaysID;
                tempSchedule.Day = appData.JVDay;
                tempSchedule.EndTime = appData.JVEndTime;
                tempSchedule.EndTimeRaw = new System.DateTime();
                tempSchedule.Faculty = appData.JVFaculty;
                tempSchedule.Room = appData.JVRoom;
                // tempSchedule.Semester = appData.JVSemester;
                tempSchedule.StartTime = appData.JVStartTime;
                tempSchedule.StartTimeRaw = new System.DateTime();
                temp.Add(tempSchedule);
            }


            return temp;
        }


        private static CoursesNWGET.RootObject generateStudentSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetStudentScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<CoursesNWGET.RootObject>(response.Content);
        }

        // Old way with SQL
        //public virtual List<TeacherModel> GetTeachers()
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeachers]");
        //    return this.Database.SqlQuery<TeacherModel>("[Schoology].[GetTeachers]").ToList();
        //}

        public static List<TeacherModel> GetTeachers()
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            TeacherNW.RootObject root = getAllTeacherRootObject("https://master-api1.nextworld.net/v2/JVSchoologyTeachersResult/");

            temp = convertRootObjectTeacherIntoModel(root);

            return temp;

        }

        private static List<TeacherModel> convertRootObjectTeacherIntoModel(TeacherNW.RootObject root)
        {
            List<TeacherModel> temp = new List<TeacherModel>();

            List<TeacherNW.Record> records = new List<TeacherNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }
        private static TeacherModel covertTeacherRecordtoOldModel(TeacherNW.AppData appData)
        {
            TeacherModel temp = new TeacherModel();

            temp.Department = appData.JVDepartment;
            temp.EA7RecordsID = appData.JVEA7RecordsID;
            temp.Email = appData.EmailAddress;
            temp.FacultyID = appData.JVFacultyID;
            temp.FirstName = appData.JVFirstName;
            temp.LastName = appData.JVLastName;
            temp.SchoolID = appData.JVSchoolIDForFacutly;

            return temp;
        }
        private static TeacherNW.RootObject getAllTeacherRootObject(string url)
        {
            var client = new RestClient(url);
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            TeacherNW.RootObject root = new TeacherNW.RootObject();

            var response = client.Execute(request);
            // Writes to the console the responce we got from the HTTP request
            //Console.WriteLine(response.Content);

            root = JsonConvert.DeserializeObject<TeacherNW.RootObject>(response.Content);
            //Console.WriteLine("There are " + root.Data.records.Count + " number of records loaded");

            return root;
        }

        // Old way with sql
        //public virtual List<TeacherScheduleModel> GetTeacherSchedule(int id)
        //{
        //    //log.InfoFormat("[Executing SP]: {0}", "[Schoology].[GetTeacherSchedule] " + id);
        //    return this.Database.SqlQuery<TeacherScheduleModel>("[Schoology].[GetTeacherSchedule] " + id).ToList();
        //}

        public static List<TeacherScheduleModel> GetTeacherSchedule(int eA7RecordsId)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();


            TeacherScheduleNW.RootObject root = generateTeacherSchedule(eA7RecordsId);

            temp = convertRootObjectTeacherScheduleIntoModel(root);

            return temp;
        }

        private static TeacherScheduleNW.RootObject generateTeacherSchedule(int? eA7RecordsId)
        {
            var client = new RestClient("https://master-api1.nextworld.net/v2/JVGetTeacherScheduleResult?nwFilter=%7B%22$and%22:%5B%7B%22JVEA7RecordsID%22:%7B%22$gte%22:" + eA7RecordsId + "%7D%7D,%7B%22JVEA7RecordsID%22:%7B%22$lte%22:" + eA7RecordsId + "%7D%7D%5D%7D");
            var request = new RestRequest(Method.GET);
            client.Authenticator = auth;

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<TeacherScheduleNW.RootObject>(response.Content);
        }

        private static List<TeacherScheduleModel> convertRootObjectTeacherScheduleIntoModel(TeacherScheduleNW.RootObject root)
        {
            List<TeacherScheduleModel> temp = new List<TeacherScheduleModel>();

            List<TeacherScheduleNW.Record> records = new List<TeacherScheduleNW.Record>();
            records = root.Data.records;

            for (int i = 0; i < records.Count; i++)
            {
                temp.Add(covertTeacherScheduleRecordtoOldModel(records.ElementAt(i).appData));
            }

            return temp;
        }

        private static TeacherScheduleModel covertTeacherScheduleRecordtoOldModel(TeacherScheduleNW.AppData appData)
        {
            TeacherScheduleModel temp = new TeacherScheduleModel();

            temp.BlockColor = appData.JVBlockColor;
            temp.BlockColorRGB = appData.JVBlockColorRGB;
            temp.ClassName = appData.JVClassName;
            temp.Course = appData.JVCourse;
            temp.CourseSection = appData.JVCourseSection;
            //temp.CycleDaysID = appData.JVCycleDaysID;
            temp.Day = appData.JVDay;
            temp.EndTime = appData.JVEndTime;
            temp.EndTimeRaw = new System.DateTime();
            temp.StartTime = appData.JVStartTime;
            temp.StartTimeRaw = new System.DateTime();

            return temp;
        }
    }
}
