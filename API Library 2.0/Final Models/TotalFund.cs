﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    public partial class TotalFund
    {
        public int id { get; set; }
        public System.Guid guid { get; set; }
        public int solicitorId { get; set; }
        public Nullable<decimal> funds { get; set; }
        public string notes { get; set; }
        public string name { get; set; }
    }
}
