﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Final_Models
{
    class CourcesNW
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string JVDay { get; set; }
            public string JVCourse { get; set; }
            public string JVEndTime { get; set; }
            public bool znwLocked { get; set; }
            public string JVSemester { get; set; }
            public string JVClassName { get; set; }
            public string JVStartTime { get; set; }
            public string JVBlockColor { get; set; }
            public int JVEndTimeRaw { get; set; }
            public int JVCycleDaysID { get; set; }
            public int JVEA7RecordsID { get; set; }
            public int JVStartTimeRaw { get; set; }
            public string JVCourseSection { get; set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
            public string JVRoom { get; set; }
            public string JVFaculty { get; set; }
            public string JVBlockColorRGB { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
