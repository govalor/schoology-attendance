﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class Attendance
    {
        public Attendance() { }

        //authentication client
        HttpBasicAuthenticator authentication = new HttpBasicAuthenticator("Cheyanne.Miller@nextworld.net", "Pion33rs!");
        private string origURLGrades = "https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken?nwPaging=%7B%22limit%22:15,%22offset%22:0%7D";
        private string urlBaseGrades = "https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken?nwPaging=%7B%22limit%22:15,%22offset%22:";
        private string urlEnd = "%7D";

        public List<GetFacultyClassAttendanceTaken_Result> getStudentGrades()
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toAttendanceCourses(getRoot);

            return list;
        }

        public List<GetFacultyClassAttendanceTaken_Result> getStudentGradesLoop()
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient(origURLGrades);
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            list = toAttendanceCourses(getRoot);

            //Loop through the grades api and concatenate all the grades for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = urlBaseGrades + nextOffset + urlEnd;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);
                client.Authenticator = authentication;

                IRestResponse response1 = client.Execute(request);

                getRoot = new AttendanceCourse.RootObject();
                getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response1.Content);

                list = list.Concat(toAttendanceCourses(getRoot)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        public List<GetFacultyClassAttendanceTaken_Result> toAttendanceCourses(AttendanceCourse.RootObject root)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            List<AttendanceCourse.Record> courses = root.Data.records;

            GetFacultyClassAttendanceTaken_Result course;
            AttendanceCourse.Record record;

            for (int i = 0; i < courses.Count(); i++)
            {
                course = new GetFacultyClassAttendanceTaken_Result();
                record = courses.ElementAt(i);

                course.BlockColor = record.appData.CMBlockColor;
                course.CourseCode = record.appData.CMCourseCode;
                course.CourseName = record.appData.CMCourse;
                course.FacultyEmail = record.appData.CMEmail;
                course.FacultyId = Int32.Parse(record.appData.CMFacultyID);
                course.FacultyName = record.appData.CMFaculty;
                course.SectionCode = Int32.Parse(record.appData.CMSectionCode);
                course.SectionTitle = record.appData.CMSectionTitle;

                list.Add(course);
            }

            return list;
        }

        public List<GetFacultyClassAttendanceTaken_Result> getStudentGradesByID(Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            /*
             * The following is the execution of the get reqeust to retrieve the student grade records from Nextworld.
             * 
             * The data is stored in the NWStudentGradesModel, and needs to be translated into a list of StudentGradesModel for use.
             */
            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //Execute the request
            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            //The following is the translation to List<StudentGradesModel> to be used in the custom application 
            list = toCoursesByID(getRoot, id);

            return list;
        }

        public List<GetFacultyClassAttendanceTaken_Result> getStudentGradesByIDLoop(Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            AttendanceCourse.RootObject getRoot = new AttendanceCourse.RootObject();

            var client = new RestClient("https://master-api1.nextworld.net/v2/CMFacultyClassAttendanceTaken");
            client.Authenticator = authentication;

            var request = new RestRequest(Method.GET);

            //Execute the initial request
            IRestResponse response = client.Execute(request);

            getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response.Content);

            list = toCoursesByID(getRoot, id);

            //Loop through the grades api and concatenate all the grades for later use
            int endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            String nextURL = "";
            String nextOffset = "";

            while (endTest < getRoot.Data.pageData.total)
            {
                //find next url
                nextOffset = endTest.ToString();

                nextURL = urlBaseGrades + nextOffset + urlEnd;

                //get the next set of data from Nextworld
                client = new RestClient(nextURL);
                client.Authenticator = authentication;

                IRestResponse response1 = client.Execute(request);

                getRoot = new AttendanceCourse.RootObject();
                getRoot = JsonConvert.DeserializeObject<AttendanceCourse.RootObject>(response1.Content);

                list = list.Concat(toCoursesByID(getRoot, id)).ToList();

                //update the end test
                endTest = getRoot.Data.pageData.offset + getRoot.Data.pageData.limit;
            }

            return list;
        }

        public List<GetFacultyClassAttendanceTaken_Result> toCoursesByID(AttendanceCourse.RootObject root, Nullable<int> id)
        {
            List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

            List<AttendanceCourse.Record> courses = root.Data.records;

            GetFacultyClassAttendanceTaken_Result course;
            AttendanceCourse.Record record;

            for (int i = 0; i < courses.Count(); i++)
            {
                course = new GetFacultyClassAttendanceTaken_Result();
                record = courses.ElementAt(i);

                if (record.appData.CMFacultyID.Equals(id))
                {
                    course.BlockColor = record.appData.CMBlockColor;
                    course.CourseCode = record.appData.CMCourseCode;
                    course.CourseName = record.appData.CMCourse;
                    course.FacultyEmail = record.appData.CMEmail;
                    course.FacultyId = Int32.Parse(record.appData.CMFacultyID);
                    course.FacultyName = record.appData.CMFaculty;
                    course.SectionCode = Int32.Parse(record.appData.CMSectionCode);
                    course.SectionTitle = record.appData.CMSectionTitle;

                    list.Add(course);
                }
            }
            return list;
        }
    }
}
