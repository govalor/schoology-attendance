﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0.Translation_Models
{
    class StudentNW
    {
        public class AppData
        {
            public string nwId { get; set; }
            public string JVEmail { get; set; }
            public string JVTitle { get; set; }
            public string JVGender { get; set; }
            public bool znwLocked { get; set; }
            public string JVLastName { get; set; }
            public string JVNickName { get; set; }
            public string JVPassword { get; set; }
            public string JVFirstName { get; set; }
            public string JVGradeLevel { get; set; }
            public string JVGroupEmail { get; set; }
            public string JVMiddleName { get; set; }
            public string JVOrgUnitPath { get; set; }
            public int JVEA7RecordsID { get; set; }
            public string JVUserDefinedID { get; set; }
            public int JVClassOf { get; internal set; }
            public DateTime nwCreatedDate { get; set; }
            public DateTime nwLastModifiedDate { get; set; }
            public string nwTenantStripe { get; set; }
            public string nwCreatedByUser { get; set; }
            public string nwLastModifiedByUser { get; set; }
        }

        public class UIHint
        {
            public string UIHint1 { get; set; }
        }

        public class UserInterfaceHint
        {
            public string TableSchema { get; set; }
            public object Field { get; set; }
            public object HeaderDetail { get; set; }
            public object SubTableField { get; set; }
            public List<UIHint> UIHints { get; set; }
        }

        public class Record
        {
            public string version { get; set; }
            public string nateDisposition { get; set; }
            public AppData appData { get; set; }
            public List<UserInterfaceHint> UserInterfaceHints { get; set; }
        }

        public class PageData
        {
            public int offset { get; set; }
            public int limit { get; set; }
            public int total { get; set; }
        }

        public class Data
        {
            public string nwTable { get; set; }
            public List<Record> records { get; set; }
            public PageData pageData { get; set; }
        }

        public class RootObject
        {
            public List<object> DebugTrace { get; set; }
            public List<object> ErrorMessages { get; set; }
            public List<object> UserInterfaceHints { get; set; }
            public Data Data { get; set; }
        }
    }
}
