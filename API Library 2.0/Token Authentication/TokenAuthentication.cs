﻿using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Library_2._0
{
    public class TokenAuthentication
    {
        private static string USERNAME = "";
        private static string PASSWORD = "";
        private static string TOKEN_URL = "https://auth1.nextworld.net/Global/API/Authenticate/Tokens";

        public TokenAuthentication(string username, string password)
        {
            USERNAME = username;
            PASSWORD = password;
        }

        public string getToken(string zone, string lifecycle)
        {
            Token.RootObject root = new Token.RootObject();
            Authentication.RootObject postRoot = new Authentication.RootObject();

            //indicate desired zone and lifecycle
            postRoot.lifecycle = lifecycle;
            postRoot.zone = zone;

            //create and execute request
            RestClient client = new RestClient(TOKEN_URL);
            client.Authenticator = new HttpBasicAuthenticator(USERNAME, PASSWORD);

            IRestRequest request = new RestRequest(Method.POST);
            request.AddParameter("application/json", JsonConvert.SerializeObject(postRoot), ParameterType.RequestBody);

            IRestResponse response = client.Execute(request);

            //deserialize JSON into model
            root = JsonConvert.DeserializeObject<Token.RootObject>(response.Content);

            return root.access_token;
        }
    }
}
