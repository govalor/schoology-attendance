﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Schoology.Api.Entities.Models;

namespace Schoology.Api.Entities.Models
{
    public class Groups
    {
        public List<Group> group { get; set; }
    }

    public class GroupStatus
    {
        public int status_code { get; set; }
        public Groups groups { get; set; }
    }

    public class GroupApiJson
    {
        public Groups groups { get; set; }
    }

    public class GroupStatuses
    {
        public List<GroupStatus> status { get; set; }
    }

    public class GroupDate
    {
        public string date { get; set; }
        public GroupStatuses statuses { get; set; }
    }

    public class GroupTotal
    {
        public int status { get; set; }
        public int count { get; set; }
    }

    public class GroupTotals
    {
        public List<GroupTotal> total { get; set; }
    }

    public class GroupRoot
    {
        public int group { get; set; }
        public string total { get; set; }
        public string links { get; set; }
    }

    public class Options
    {
        public int member_post { get; set; }
        public int member_post_comment { get; set; }
        public int create_discussion { get; set; }
        public int create_files { get; set; }
        public int invite_type { get; set; }
    }

    public class Group
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string website { get; set; }
        public object access_code { get; set; }
        public string category { get; set; }
        public Options options { get; set; }
        public string group_code { get; set; }
        public string privacy_level { get; set; }
        public string picture_url { get; set; }
        public string school_id { get; set; }
        public Links links { get; set; }
    }

    public class SchoologyGroup
    {
        public List<Group> group { get; set; }
    }

    public class Links
    {
        public string self { get; set; }
    }

    public class sEnrollment
    {
        public string id { get; set; }
        public string uid { get; set; }
        public string school_uid { get; set; }
        public string name_title { get; set; }
        public string name_title_show { get; set; }
        public string name_first { get; set; }
        public string name_first_preferred { get; set; }
        public string use_preferred_first_name { get; set; }
        public string name_middle { get; set; }
        public string name_middle_show { get; set; }
        public string name_last { get; set; }
        public string name_display { get; set; }
        public int admin { get; set; }
        public string status { get; set; }
        public string picture_url { get; set; }
        public Links links { get; set; }
    }

    public class RootEnrollment
    {
        public List<sEnrollment> enrollment { get; set; }
        public int total { get; set; }
        public Links links { get; set; }
    }

    public class GroupNames
    {
        public int id { get; set; }
        public string schoologygroupid { get; set; }
        public string groupcategory { get; set; }
        public string grouptitle { get; set; }
    }

    public class EnrollmentList
    {
        public int id { get; set; }
        public string groupid { get; set; }
        public string grouptitle { get; set; }
        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string sid { get; set; }
        public string uid { get; set; }
    }

    public class GetGroups_Result
    {
        public string groupid { get; set; }
        public string groupname { get; set; }
    }

    public class GetEnrollment_Result
    {
        public int id { get; set; }
        public string groupid { get; set; }
        public string grouptitle { get; set; }
        public string name { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string sid { get; set; }
        public string uid { get; set; }
    }

    public class SchoologyUser
    {
        public string uid { get; set; }
        public int id { get; set; }
        public int school_id { get; set; }
        public int synced { get; set; }
        public string school_uid { get; set; }
        public int building_id { get; set; }
        public string additional_buildings { get; set; }
        public string name_title { get; set; }
        public int name_title_show { get; set; }
        public string name_first { get; set; }
        public string name_first_preferred { get; set; }
        public string name_middle { get; set; }
        public int name_middle_show { get; set; }
        public string name_last { get; set; }
        public string name_display { get; set; }
        public string username { get; set; }
        public string primary_email { get; set; }
        public string picture_url { get; set; }
        public object gender { get; set; }
        public object position { get; set; }
        public string grad_year { get; set; }
        public string password { get; set; }
        public int role_id { get; set; }
        public int tz_offset { get; set; }
        public string tz_name { get; set; }
        public object parents { get; set; }
        public object child_uids { get; set; }
    }

    public class RootSchoologyUser    {
        public List<SchoologyUser> user { get; set; }
        public string total { get; set; }
        public Links links { get; set; }
    }
}
