﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Entities.Models
{

    public class AssignmentLinks
    {
        public string self { get; set; }
    }

    public class Assignment
    {
        public int id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string due { get; set; }
        public string grading_scale { get; set; }
        public string grading_period { get; set; }
        public string grading_category { get; set; }
        public string max_points { get; set; }
        public string factor { get; set; }
        public string is_final { get; set; }
        public string show_comments { get; set; }
        public string grade_stats { get; set; }
        public string allow_dropbox { get; set; }
        public string allow_discussion { get; set; }
        public int published { get; set; }
        public string type { get; set; }
        public int grade_item_id { get; set; }
        public int available { get; set; }
        public int completed { get; set; }
        public int dropbox_locked { get; set; }
        public int grading_scale_type { get; set; }
        public bool show_rubric { get; set; }
        public int num_assignees { get; set; }
        public List<int> assignees { get; set; }
        public List<int> grading_group_ids { get; set; }
        public string completion_status { get; set; }
        public AssignmentLinks links { get; set; }
    }

    public class AssignmentLinks2
    {
        public string self { get; set; }
    }

    public class AssignmentRoot
    {
        public List<Assignment> assignment { get; set; }
        public int total { get; set; }
        public AssignmentLinks2 links { get; set; }
    }

}
