﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.Attendance.Models
{
    public class StudentAttendanceByClassModel
    {
        public long SectionId { get; set; }
        public long EnrollmentId { get; set; }
        public string AttendanceDate { get; set; }
        public string Class { get; set; }
        public string BlockColor { get; set; }
        public string Period { get; set; }
        public int Status { get; set; }
        public string Comment { get; set; }
        public int SortOrder { get; set; }
    }
}