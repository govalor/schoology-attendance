﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.Attendance.Models
{
    public class AttendanceReportModel
    {
        public string Date { get; set; }
        public List<AttendanceModel> AttendanceModelList { get; set; }
    }
    public class AttendanceModel
    {
        public DateTime Date { get; set; }
        public string UID { get; set; }
        public int SchoolUID { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<AttendanceStudentSectionModel> AttendanceStudentSectionModel { get; set; }
    }

    public class AttendanceStudentSectionModel
    {
        public string CourseName { get; set; }
        public string CourseCode { get; set; }
        public string SectionTitle { get; set; }
        public string SectionCode { get; set; }
        public string BlockColor { get; set; }
        public string RoomNumber { get; set; }
        public String Date { get; set; }
        public int AttendanceCode { get; set; }
        public string AttendanceCodeTitle { get; set; }
        public string AttendanceComment { get; set; }
    }
}