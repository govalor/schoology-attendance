﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.Attendance.Models
{
    public class GridMVCModel
    {
        public int GridPage { get; set; }
        public string GridFilter { get; set; }
        public string GridColumn { get; set; }
        public int GridDir { get; set; }
        public string SearchString { get; set; }

    }
}