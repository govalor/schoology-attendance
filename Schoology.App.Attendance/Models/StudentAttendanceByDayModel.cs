﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Schoology.App.Attendance.Models
{
    public class StudentAttendanceByDayModel
    {
        public string Date { get; set; }
        public string CycleDay { get; set; } // Monday, Tuesday, Wednesday, Thursday, Friday
        public int Status { get; set; }
        public string Comment { get; set; }

    }
}