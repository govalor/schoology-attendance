﻿using Newtonsoft.Json;
using Schoology.Api.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Schoology.Api.Entities.Models;
using System.Configuration;
using Schoology.App.Attendance.Models;
using System.Net;
using RazorPDF;

namespace Schoology.App.Attendance.Controllers
{
    public class GlobalController : Controller
    {
        private static List<GradingPeriod> _gradingPeriodList = new List<GradingPeriod>();
        private static List<Course> _totalCourseList { get; set; }
        private static List<Course> _filterCourseList { get; set; }

        /// <summary>
        /// Loads the Courses and Sections for both Global Attendance by All Day and by Time.
        /// </summary>
        /// <param name="searchString">Find by Name SearchString to filter the Course Listing.</param>
        /// <returns></returns>
///        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        private ActionResult LoadCourseSections(string searchString)
        {
            // Create the list to hold the users.
            List<Course> filterList = new List<Course>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {
                // Get the users from Schoology
                _totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }

            }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    _filterCourseList = _totalCourseList.Where(s =>

                        s.title.ToLower().Contains(searchString.ToLower()) ||
                        s.course_code.ToLower().Contains(searchString.ToLower()) ||
                        s.section.Any(c => c.section_title.ToLower().Contains(searchString.ToLower()))

                        ).ToList();

                }
            }
            else
            {
                _filterCourseList = _totalCourseList;
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            return View(_filterCourseList);
        }

        /// <summary>
        /// Loads the courses and section for Global Attendance by All Day.
        /// </summary>
        /// <param name="searchString">Find by Name SearchString to filter the Course Listing.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult AllDayIndex(string searchString)
        {
            return LoadCourseSections(searchString);
        }

        /// <summary>
        /// Loads the courses and sections for Global Attendance by Time.
        /// </summary>
        /// <param name="searchString">Find by Name SearchString to filter the Course Listing.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult TimeIndex(string searchString)
        {
            return LoadCourseSections(searchString);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult AllDayAttendance(long id, DateTime currentDate)
        {
            SetGridMVCViewBag();

            //string sectionValidationSummary = SectionService.Instance.ValidateSection(id);
            //if(!string.IsNullOrEmpty(sectionValidationSummary))
            //    return RedirectToAction("Error", new { errorMessage = sectionValidationSummary, actionName = "AllDayIndex" });

            // We only want to display the class attendance on the days that the class is scheduled.
            // So we need to find out when the class is scheduled based on days and set the current date in the
            // datepicker to the closest day. The datepicker will take care of the rest by disabling the days
            // that the class does not meet.

            // The currentDate is set the the current date when this page is first accessed.
            // The user can change the date and will post back to this method.

            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday
            // Disable the JQuery Datepicker days the class is not scheduled on.
            string daysToDisable = "0,6";
            ViewBag.daysToDisable = daysToDisable;

            // Check the date to when the class meets. If the date is on a disabled date, then find the next date the class is schedule on.
            // This will only happen the first time this page is accessed.
            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday

            int dayOfWeekNumber = (int)currentDate.DayOfWeek;
            int nextDayOfWeekNumber;
            //DateTime date = currentDate;

            if (daysToDisable.Contains(dayOfWeekNumber.ToString()))
            {
                nextDayOfWeekNumber = (dayOfWeekNumber == 5) ? dayOfWeekNumber = 1 : dayOfWeekNumber++;
                currentDate = ClassSchedule.GetNextDateForDay(DateTime.Now, (DayOfWeek)Enum.Parse(typeof(DayOfWeek), nextDayOfWeekNumber.ToString()));
            }


            string searchDate = currentDate.ToString("yyyy-MM-dd");

            ViewBag.searchDate = searchDate;

            ViewBag.AttendanceCode = AttendanceService.Instance.GetAttendanceCodes();



            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));

            Section section = course.section.Single(x => x.id == id);

            //User user = _totalList.Where(x => x.id == id);
            //Course course = _totalCourseList.Single(x => x.id == id);

            //List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollments(section.id, false, false);
            // Json File Call
            // For increase proformance
            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, true);

            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            // Get the teachers. Used for roster printing.
            long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
            section.teachers = enrollmentList.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            // Set the students for the section.
            section.enrollments = enrollmentList;


            // Create one attendance record a initials for the form.
            // Based on the dates, if there is more than one we will duplicate the attendance record for the other dates.
            foreach (Enrollment enrollment in section.enrollments)
            {
                List<Schoology.Api.Entities.Models.Attendance> empyList = new List<Schoology.Api.Entities.Models.Attendance>();

                Schoology.Api.Entities.Models.Attendance attendance = new Schoology.Api.Entities.Models.Attendance();

                // Initialize the attendance record
                attendance.enrollment_id = enrollment.id;
                attendance.comment = "";
                attendance.status = -1;
                attendance.date = searchDate;

                empyList.Add(attendance);

                enrollment.attendances = empyList;

            }

            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="section"></param>
        /// <param name="selectAllAttendanceCodes"></param>
        /// <param name="selectAllComment"></param>
        /// <param name="fc"></param>
        /// <param name="sid"></param>
        /// <param name="checkAll"></param>
        /// <returns></returns>
   ///     [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult AllDayAttendance(long id, string startDate, string endDate, Section section, string selectAllAttendanceCodes, string selectAllComment, FormCollection fc, string[] sid, bool checkAll)
        {

            SetGridMVCViewBag();

            int selectAllAttendanceCode = !string.IsNullOrEmpty(selectAllAttendanceCodes) ? Convert.ToInt32(selectAllAttendanceCodes) : 0;

            //int selectAllAttendanceCode = !string.IsNullOrEmpty(fc["SelectAllAttendanceCodes"]) ? Convert.ToInt32(fc["SelectAllAttendanceCodes"]) : 0;

            //string selectAllComment = !string.IsNullOrEmpty(fc["SelectAllComment"]) ? fc["SelectAllComment"] : "";

            List<Enrollment> filterEnrollment;

            // If Global select do this
            if (checkAll)
            {
                // Convert the checkboxes that are checked to a list of enrollment ids.
                List<string> list2 = sid.ToList();
                // Filter out the section enrollements from the ones that were checked.
                filterEnrollment = section.enrollments.FindAll(emp => list2.Contains(emp.id.ToString()));

            }
            else
            {
                filterEnrollment = section.enrollments;
            }

            if (filterEnrollment != null)
            {

                // Make sure attendance code was selected.

                if (selectAllAttendanceCode > 0)
                {
                    int blackBaudCode = 0;
                    int schoologyStatusCode = 0;

                    // Separate the attendance code.
                    blackBaudCode = selectAllAttendanceCode % 10;
                    schoologyStatusCode = selectAllAttendanceCode / 10;

                    // Set the schoology attendance code (1, 2, 3, 4)
                    selectAllAttendanceCode = schoologyStatusCode;

                    string comment = "";

                    foreach (Enrollment enrollment in filterEnrollment)
                    {
                        // Black out the comment so we don't keep concatenating the previous enrollment record.
                        comment = selectAllComment;

                        // Save attendance for user using the API version
                        //string returnValue = UserService.Instance.PutUserSectionsAttendanceApi(enrollment.uid, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), selectAllAttendanceCode, selectAllComment);

                        // Save attendance for user using the Json file version

                        //// Separate the attendance code.
                        //blackBaudCode = selectAllAttendanceCode % 10;
                        //schoologyStatusCode = selectAllAttendanceCode / 10;

                        //// Set the schoology attendance code (1, 2, 3, 4)
                        //selectAllAttendanceCode = schoologyStatusCode;



                        // BUG: To fix the date problem..
                        if (selectAllAttendanceCode == 1)
                        {
                            if (string.IsNullOrEmpty(comment))
                            {
                                comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                            }

                        }
                        else
                        {
                            // BUG: To fix the date problem.
                            if (string.IsNullOrEmpty(comment))
                            {
                                comment = " ";
                            }

                            comment = blackBaudCode.ToString() + comment;

                        }

                        string returnValue = UserService.Instance.PutUserSectionsAttendanceJson(enrollment.uid, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), selectAllAttendanceCode, comment);


                        // TODO:
                        // Check return value of Http 200 - Success

                    }
                }
            }

            return RedirectToAction("AllDayAttendance", new { id = id, currentDate = DateTime.Now });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult TimeAttendance(long id, DateTime currentDate)
        {

            SetGridMVCViewBag();

            //string sectionValidationSummary = SectionService.Instance.ValidateSection(id);
            //if (!string.IsNullOrEmpty(sectionValidationSummary))
            //    return RedirectToAction("Error", new { errorMessage = sectionValidationSummary, actionName = "TimeIndex" });

            // We only want to display the class attendance on the days that the class is scheduled.
            // So we need to find out when the class is scheduled based on days and set the current date in the
            // datepicker to the closest day. The datepicker will take care of the rest by disabling the days
            // that the class does not meet.

            // The currentDate is set the the current date when this page is first accessed.
            // The user can change the date and will post back to this method.

            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday
            // Disable the JQuery Datepicker days the class is not scheduled on.
            string daysToDisable = "0,6";
            ViewBag.daysToDisable = daysToDisable;

            // Check the date to when the class meets. If the date is on a disabled date, then find the next date the class is schedule on.
            // This will only happen the first time this page is accessed.
            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday

            int dayOfWeekNumber = (int)currentDate.DayOfWeek;
            int nextDayOfWeekNumber;
            //DateTime date = currentDate;

            if (daysToDisable.Contains(dayOfWeekNumber.ToString()))
            {
                nextDayOfWeekNumber = (dayOfWeekNumber == 5) ? dayOfWeekNumber = 1 : dayOfWeekNumber++;
                currentDate = ClassSchedule.GetNextDateForDay(DateTime.Now, (DayOfWeek)Enum.Parse(typeof(DayOfWeek), nextDayOfWeekNumber.ToString()));
            }


            string searchDate = currentDate.ToString("yyyy-MM-dd");

            ViewBag.searchDate = searchDate;

            ViewBag.AttendanceCode = AttendanceService.Instance.GetAttendanceCodes();

            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));

            Section section = course.section.Single(x => x.id == id);

            //User user = _totalList.Where(x => x.id == id);
            //Course course = _totalCourseList.Single(x => x.id == id);

            //List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollments(section.id, false, false);

            // Json File Call
            // For increase proformance
            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, true);


            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            // Get the teachers. Used for roster printing.
            long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
            section.teachers = enrollmentList.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            // Set the students for the section.
            section.enrollments = enrollmentList;


            // Create one attendance record a initials for the form.
            // Based on the dates, if there is more than one we will duplicate the attendance record for the other dates.
            foreach (Enrollment enrollment in section.enrollments)
            {
                List<Schoology.Api.Entities.Models.Attendance> empyList = new List<Schoology.Api.Entities.Models.Attendance>();

                Schoology.Api.Entities.Models.Attendance attendance = new Schoology.Api.Entities.Models.Attendance();

                // Initialize the attendance record
                attendance.enrollment_id = enrollment.id;
                attendance.comment = "";
                attendance.status = -1;
                attendance.date = searchDate;

                empyList.Add(attendance);

                enrollment.attendances = empyList;

            }

            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="startDate"></param>
        /// <param name="section"></param>
        /// <param name="selectAllAttendanceCodes"></param>
        /// <param name="selectAllComment"></param>
        /// <param name="fc"></param>
        /// <param name="sid"></param>
        /// <param name="checkAll"></param>
        /// <param name="classScheduleId"></param>
        /// <param name="classScheduleCheckAll"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult TimeAttendance(long id, string startDate, Section section, string selectAllAttendanceCodes, string selectAllComment, FormCollection fc, string[] sid, bool checkAll, string[] classScheduleId, bool classScheduleCheckAll)
        {

            SetGridMVCViewBag();

            int selectAllAttendanceCode = !string.IsNullOrEmpty(selectAllAttendanceCodes) ? Convert.ToInt32(selectAllAttendanceCodes) : 0;

            //int selectAllAttendanceCode = !string.IsNullOrEmpty(fc["SelectAllAttendanceCodes"]) ? Convert.ToInt32(fc["SelectAllAttendanceCodes"]) : 0;

            //string selectAllComment = !string.IsNullOrEmpty(fc["SelectAllComment"]) ? fc["SelectAllComment"] : "";

            List<Enrollment> filterEnrollment;

            // If Global select do this
            if (checkAll)
            {
                // Convert the checkboxes that are checked to a list of enrollment ids.
                List<string> list2 = sid.ToList();
                // Filter out the section enrollements from the ones that were checked.
                filterEnrollment = section.enrollments.FindAll(emp => list2.Contains(emp.id.ToString()));

            }
            else
            {
                filterEnrollment = section.enrollments;
            }

            if (filterEnrollment != null)
            {
                // Make sure attendance code was selected.

                if (selectAllAttendanceCode > 0 && sid != null && classScheduleId != null)
                {
                    int blackBaudCode = 0;
                    int schoologyStatusCode = 0;

                    // Separate the attendance code.
                    blackBaudCode = selectAllAttendanceCode % 10;
                    schoologyStatusCode = selectAllAttendanceCode / 10;

                    // Set the schoology attendance code (1, 2, 3, 4)
                    selectAllAttendanceCode = schoologyStatusCode;

                    string comment = "";

                    foreach (Enrollment enrollment in filterEnrollment)
                    {
                        comment = selectAllComment;

                        // Save attendance for user using the API version
                        //string returnValue = UserService.Instance.PutUserSectionsAttendanceApi(enrollment.uid, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate), selectAllAttendanceCode, selectAllComment);

                        //// Separate the attendance code.
                        //blackBaudCode = selectAllAttendanceCode % 10;
                        //schoologyStatusCode = selectAllAttendanceCode / 10;

                        //// Set the schoology attendance code (1, 2, 3, 4)
                        //selectAllAttendanceCode = schoologyStatusCode;

                                              // BUG: To fix the date problem..
                        if (selectAllAttendanceCode == 1)
                        {
                            if (string.IsNullOrEmpty(comment))
                            {
                                comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                            }

                        }
                        else
                        {

                            // BUG: To fix the date problem.
                            if (string.IsNullOrEmpty(comment))
                            {
                                comment = " ";
                            }

                            comment = blackBaudCode.ToString() + comment;

                        }

                        // Save attendance for user using the Json file version
                        string returnValue = UserService.Instance.PutUserSectionsAttendanceByTimeJson(enrollment.uid, Convert.ToDateTime(startDate), selectAllAttendanceCode, comment, classScheduleId);

                        // TODO:
                        // Check return value of Http 200 - Success

                    }
                }
            }

            return RedirectToAction("TimeAttendance", new { id = id, currentDate = DateTime.Now });

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Front Desk")]     
        [HttpPost]
        public ActionResult GetAttendanceMessage(DateTime date)
        {
            ViewBag.CalendarDate = Convert.ToDateTime(date).ToShortDateString();

            List<BlockClassSchedule> classScheduleDateList = ClassSchedule.Instance.GetSchedule(date);

            ViewBag.CalendarDate = date.ToShortDateString();

            return PartialView("_GlobalClassSchedule", classScheduleDateList);

        }

        /// <summary>
        /// 
        /// </summary>
        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="actionName"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Error(string errorMessage, string actionName)
        {
            SetGridMVCViewBag();

            ViewBag.ErrorMessage = errorMessage;
            ViewBag.ActionName = actionName;
            return View();
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult RosterPDF(long id, DateTime date)
        {
            //KDC added
            List<Course> totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            ViewBag.Date = date.ToShortDateString();

            //Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Course course = totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Section section = course.section.Single(x => x.id == id);

            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, true);

            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            // Get the teachers. Used for roster printing.
            long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
            section.teachers = enrollmentList.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

            // Get the students
            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            section.enrollments = enrollmentList;

            return new PdfActionResult("RosterPDF", section);

        }

    }
}