﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using Schoology.Api.Service;
using Schoology.Api.Entities.Models;
using System.Net;
using Schoology.App.Attendance.Models;
using log4net;

namespace Schoology.App.Attendance.Controllers
{
    public class StudentController : Controller
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static List<User> _totalUserList { get; set; }

        private List<User> LoadStudents(string searchString)
        {
            // Create the list to hold the users.
            List<User> filterUserList = new List<User>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {
                // Get the users from Schoology Json File instead of calling the Schoology API directly.
                _totalUserList = UserService.Instance.GetUsersJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }


            }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    //filterUserList = _totalUserList.Where(s => s.name_last.ToLower().Contains(searchString.ToLower())
                    //    || s.name_first.ToLower().Contains(searchString.ToLower())).ToList();


                    // Temporary solution. Need to filter only student that are current students and have a StudentID. This needs to be done in UserService.cs
                    _totalUserList = _totalUserList.Where(s => !string.IsNullOrEmpty(s.name_title)).ToList();
                        

                    //foreach(User user in _totalUserList)
                    //{
                    //    if(string.IsNullOrEmpty(user.position))
                    //    {
                    //        User temp = user;

                    //    }
                    //}

                    filterUserList = _totalUserList.Where(s => s.name_last.ToLower().Contains(searchString.ToLower())
                        || s.name_first.ToLower().Contains(searchString.ToLower())
                        || s.name_title.ToLower().Contains(searchString.ToLower())).ToList();
                }
            }
            else
            {
                filterUserList = _totalUserList;
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //List<Attendance> list = attendanceRoot.attendance.Where(x => x.date == "2015-06-03").ToList();

            return filterUserList;
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult StudentSchedulePDF(int id, string firstname, string lastname)
        {

            return Redirect("https://schoology.valorchristian.com/SchoologyAppSchedule/schedule/StudentSchedulePDF/6329?firstname=Rachel&lastname=Abraham"); // redirects to external url


            //return View(LoadStudents(searchString));
        }


        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult DayIndex(string searchString)
        {
            return View(LoadStudents(searchString));
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult ClassIndex(string searchString)
        {

            return View(LoadStudents(searchString));
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult DayAttendance(long id)
        {
            SetGridMVCViewBag();

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //User user = _totalList.Where(x => x.id == id);
            User user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);

        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult DayAttendance(long id, string startdate, string enddate, List<StudentAttendanceByDayModel> list )
        {
            SetGridMVCViewBag();

            // loop through the selected dates
            foreach(StudentAttendanceByDayModel model in list)
            {
                int blackBaudCode = 0;
                int schoologyStatusCode = 0;

                if(model.Status > 0)
                {
                    StudentAttendanceByDayModel studentAttendance = new StudentAttendanceByDayModel();

                    studentAttendance.Date = model.Date;

                    // Separate the attendance code.
                    blackBaudCode = model.Status % 10;
                    schoologyStatusCode = model.Status / 10;

                    // Set the schoology attendance code (1, 2, 3, 4)
                    studentAttendance.Status = schoologyStatusCode;


                    // BUG: To fix the date problem.
                    if (studentAttendance.Status == 1)
                    {
                        if (string.IsNullOrEmpty(model.Comment))
                        {
                            studentAttendance.Comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                        }

                    }
                    else
                    {
                        // BUG: To fix the date problem.
                        if (string.IsNullOrEmpty(model.Comment))
                        {
                            model.Comment = " ";
                        }

                        studentAttendance.Comment = blackBaudCode.ToString() + model.Comment;
                    }

                    string returnValue = UserService.Instance.PutUserSectionsAttendanceJson(
                        id,
                        Convert.ToDateTime(studentAttendance.Date),
                        studentAttendance.Status,
                        studentAttendance.Comment
                    );

                }
            }

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);

        }

        private IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult DayAttendanceGrid(string startDate, string endDate)
        {
            ViewBag.AttendanceCode = AttendanceService.Instance.GetAttendanceCodes();

            List<StudentAttendanceByDayModel> list = new List<StudentAttendanceByDayModel>();

            foreach (DateTime date in EachDay(Convert.ToDateTime(startDate), Convert.ToDateTime(endDate)))
            {
                StudentAttendanceByDayModel model = new StudentAttendanceByDayModel();

                model.Date = date.ToShortDateString();
                model.CycleDay = date.DayOfWeek.ToString();

                // TODO: We would need to check and see if all classes are set to a status code and then display that status code.
                // If at least one of the status is different, display -- Select Status Code --
                model.Status = 0;
                model.Comment = "";

                list.Add(model);
                // Do something with the date
            }

            ViewBag.StartCalendarDate = Convert.ToDateTime(startDate).ToShortDateString();
            ViewBag.EndCalendarDate =  Convert.ToDateTime(endDate).ToShortDateString();
            //ViewBag.CalendarDate = DateTime.Parse(startDate).ToShortDateString() + " " + DateTime.Parse(endDate).ToShortDateString();

            return PartialView("_DayAttendanceGrid", list);
            //return Json(new { Status = 1, Message = "Ok", Content = PartialView("_Test")});

        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult ClassAttendanceGrid(long id, string startDate)
        {

            DateTime date = Convert.ToDateTime(startDate);

            ViewBag.AttendanceCode = AttendanceService.Instance.GetAttendanceCodes();
            ViewBag.StartCalendarDate = Convert.ToDateTime(startDate).ToShortDateString();

            // Get the students class schedule
            User user = UserService.Instance.GetUserJson(id);

            //List<Section> userSectionList = UserService.Instance.GetUserSectionsJson(id, true, true);
            List<Section> userSectionList = user.sections;


            // Get only the sections by the date
            int day = (int)date.DayOfWeek;
            userSectionList = userSectionList.Where(x => x.meeting_days.Contains(day.ToString())).ToList();



            //************MODIFIED DAYS**************/
            // KDC Temporary New code for modified days
            userSectionList = UserService.Instance.GetUserSections(id);



            // Get the Period description
            List<BlockClassSchedule> blockClassScheduleList = ClassSchedule.Instance.GetSchedule(date);
            //List<string> blockTimes = blockClassScheduleList.Select(x => x.Time).Distinct().ToList();

            List<StudentAttendanceByClassModel> list = new List<StudentAttendanceByClassModel>();

            foreach (Section section in userSectionList)
            {
                //string sectionValidationSummary = SectionService.Instance.ValidateSection(section.id);
                //if (!string.IsNullOrEmpty(sectionValidationSummary))
                //    return RedirectToAction("Error", new { errorMessage = sectionValidationSummary, actionName = "ClassIndex" });

                StudentAttendanceByClassModel model = new StudentAttendanceByClassModel();

                //string blockColor = ClassSchedule.Instance.BlockColor(section.description);
                string blockColor = ClassSchedule.Instance.BlockColor(section.section_title);
                if (!string.IsNullOrEmpty(blockColor))
                {
                    // Substring will error out if section_title is incorrect.
                    try
                    {
                        model.Class = section.course_code + "-" + section.section_title.Substring(8);
                    }
                    catch (Exception ex)
                    {
                        model.Class = section.course_code;
                        log.ErrorFormat("[SECTION] - Invalid Section Title - {0}", section.section_title);
                    }

                    //string classPeriod = ClassSchedule.Instance.ClassPeriod(section.description, date);
                    string classPeriod = ClassSchedule.Instance.ClassPeriod(section.section_title, date);


                    //
                    // Get the students attendance and comment for each class
                    //
                    // https://api.schoology.com/v1/sections/134340721/attendance?enrollment_id=1213444770
                    // https://api.schoology.com/v1/sections/134340721/attendance?start=2015-07-20&end=2015-07-20

                    if (user.sectionsEnrollmentIds.ContainsKey(section.id))
                    {
                        
                        long enrollmentId = user.sectionsEnrollmentIds[section.id];

                        Schoology.Api.Entities.Models.Attendance attendance = new Schoology.Api.Entities.Models.Attendance();
                        attendance = AttendanceService.Instance.GetAttendanceByDate(section.id, enrollmentId, date);


                        // This is used for the post (saving of the attendance data).
                        model.SectionId = section.id;
                        model.EnrollmentId = enrollmentId;
                        model.AttendanceDate = date.ToString("yyyy-MM-dd");

                        // Display to user and set the status and comment.
                        model.BlockColor = blockColor;
                        model.Period = blockColor + " (" + classPeriod + ")";

                        if (attendance.status > 0)
                        {

                            // BUG: To fix the date problem.
                            if (attendance.status == 1)
                            {
                                model.Status = 11;
                                if (string.IsNullOrEmpty(attendance.comment))
                                {
                                    model.Comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                                }
                                else
                                {
                                    model.Comment = attendance.comment;
                                }

                            }
                            else
                            {
                                // Separate the attendance code.
                                int blackBaudCode = Convert.ToInt16(attendance.comment.Substring(0, 1)); // or comment[0]
                                int schoologyStatusCode = attendance.status;

                                model.Comment = attendance.comment.Substring(1);
                                model.Status = (schoologyStatusCode * 10) + blackBaudCode;

                            }

                        }
                        else
                        {
                            model.Status = -1;
                            model.Comment = "";
                        }

                        //model.Status = attendance.status;
                        //model.Comment = attendance.comment;

                        model.SortOrder = ClassSchedule.Instance.ClassTimeSortOrder(classPeriod);

                        list.Add(model);
                        // Do something with the date
                    }
                    else
                    {
                        // Add error handler here...
                    }
                }
            }

            List<StudentAttendanceByClassModel> sortedList = list.OrderBy(o => o.SortOrder).ToList();

            return PartialView("_ClassAttendanceGrid", sortedList);

        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult ClassAttendance(long id)
        {

            SetGridMVCViewBag();

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);

        }

        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult ClassAttendance(long id, List<StudentAttendanceByClassModel> list)
        {

            SetGridMVCViewBag();

            // Convert date from 7/18/2015 to schoology date for saving attendance: 2015-07-18
            //string startDate = ViewBag.StartCalendarDate;
            //DateTime date = Convert.ToDateTime(startDate);
            //startDate = date.ToString("yyyy-MM-dd");

            // Save the attendance records (create or update)
            List<Schoology.Api.Entities.Models.Attendance> attendanceList = new List<Schoology.Api.Entities.Models.Attendance>();

            // loop through the selected dates
            foreach (StudentAttendanceByClassModel model in list)
            {

                int blackBaudCode = 0;
                int schoologyStatusCode = 0;

                if (model.Status > 0)
                {
                    Schoology.Api.Entities.Models.Attendance attendance = new Api.Entities.Models.Attendance();
                    attendance.date = model.AttendanceDate;
                    attendance.enrollment_id = model.EnrollmentId;

                    // Separate the attendance code.
                    blackBaudCode = model.Status % 10;
                    schoologyStatusCode = model.Status / 10;

                    // Set the schoology attendance code (1, 2, 3, 4)
                    attendance.status = schoologyStatusCode;

                    // BUG: To fix the date problem.
                    if (attendance.status == 1)
                    {
                        if (string.IsNullOrEmpty(model.Comment))
                        {
                            attendance.comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                        }
                        else
                        {
                            attendance.comment = model.Comment;
                        }

                    }
                    else
                    {
                        // BUG: To fix the date problem.
                        if (string.IsNullOrEmpty(model.Comment))
                        {
                            model.Comment = " ";
                        }

                        attendance.comment = blackBaudCode.ToString() + model.Comment;
                    }

                    attendanceList.Add(attendance);

                    AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                    attendanceApiJson.attendances = new Attendances() { attendance = attendanceList };

                    if (attendanceApiJson.attendances.attendance.Count > 0)
                    {
                        string returnvalue = AttendanceService.Instance.PutAttendance(model.SectionId, attendanceApiJson);
                    }
                }

            }

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);

        }

        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }

    }
}