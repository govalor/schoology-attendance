﻿using Newtonsoft.Json;
using Schoology.Api.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Schoology.Api.Entities.Models;
using System.Configuration;
using Schoology.App.Attendance.Models;
using System.Net;
using System.Security.Claims;
using RazorPDF;
using log4net;
using Schoology.Custom.Entities.Models;
using API_Library_2._0;


namespace Schoology.App.Attendance.Controllers
{
    public class CourseController : Controller
    {
        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private SchoologyDbContext db = new SchoologyDbContext();

        //private static List<GradingPeriod> _gradingPeriodList = new List<GradingPeriod>();
        //private static List<Course> _totalCourseList { get; set; }
        //private static List<Course> _filterCourseList { get; set; }

        [Authorize(Roles = "Admin, School Admin, Teacher, Front Desk")]
        /// <summary>
        /// Display the Courses and their sections. Sorting, Filtering, and Searching are provided. Plus navigation back to where last displayed.
        /// </summary>
        /// <param name="searchString">Find by Name SearchString to filter the Course Listing.</param>
        /// <returns></returns>
        public ActionResult Index(string searchString)
        {
            long uid = 0;

            List<Course> totalCourseList;
            List<Course> filterCourseList;

            // Create the list to hold the courses.
            List<Course> filterList = new List<Course>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            //KDC - Added
            totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();
            filterCourseList = totalCourseList;

            // TODO: Make this easier to use
            // Get the current schoology user id.
            ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
            List<Claim> claims = identity.Claims.ToList();
            var claimUid = claims.Where(c => c.Type == "http://www.schoology.com/uid").Select(c => c.Value).SingleOrDefault();
            uid = Convert.ToInt64(claimUid);

            // If this is a teacher, only get the courses the teacher teaches.
            if (User.IsInRole("Teacher"))
            {
                //log.ErrorFormat("[Course] - IsInRole(Teacher) - {0} - ID - {1}", User.Identity.Name, uid);

                // Only get the courses the teacher teaches
                //List<Course> teachersList = _totalCourseList.Where(x => x.section.Any(o => o.enrollments.Any(w => w.uid == uid))).ToList();
                List<Course> teachersList = totalCourseList.Where(x => x.section.Any(o => o.enrollments.Any(w => w.uid == uid))).ToList();

                // Now get only the sections the teacher teaches.
                foreach (Course course in teachersList)
                {
                    List<Section> sectionList = course.section.Where(x => x.enrollments.Any(e => e.uid == uid)).ToList();
                    course.section = sectionList;
                }

                //_totalCourseList = teachersList;
                totalCourseList = teachersList;
            }


            // If no query strings, load the data once and store in static variable.
            if (Request.QueryString.Count == 0)
            {
                // Get the course from Schoology json file.
                //_totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();



                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }

            }

            // Filter the list by title, course code and section
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    //_filterCourseList = _totalCourseList.Where(s =>

                    //    s.title.ToLower().Contains(searchString.ToLower()) ||
                    //    s.course_code.ToLower().Contains(searchString.ToLower()) ||
                    //    s.section.Any(c => c.section_title.ToLower().Contains(searchString.ToLower()))
                        
                    //    ).ToList();

                    filterCourseList = totalCourseList.Where(s =>

                        s.title.ToLower().Contains(searchString.ToLower()) ||
                        s.course_code.ToLower().Contains(searchString.ToLower()) ||
                        s.section.Any(c => c.section_title.ToLower().Contains(searchString.ToLower()))

                        ).ToList();

                }
            }
            else
            {
                //_filterCourseList = _totalCourseList;
                filterCourseList = totalCourseList;
            }

            List<GetFacultyClassAttendanceTaken_Result> facultyAttendanceList;

            // DONT USE - SCHOOLOGY BUG WITH v1/users/me
            //User currentUser = UserService.Instance.GetCurrentUser();

            User currentUser = UserService.Instance.GetUser(uid);

            if (currentUser.school_uid == null)
            {
                facultyAttendanceList = db.GetFacultyClassAttendanceTaken().ToList();
            }
            else
            {
                facultyAttendanceList = db.GetFacultyClassAttendanceTakenById(Convert.ToInt32(currentUser.school_uid)).ToList();
            }

            foreach (Course course in filterCourseList)
            {
                foreach (Section section in course.section)
                {
                    foreach (GetFacultyClassAttendanceTaken_Result attendance in facultyAttendanceList)
                    {
                        if (Convert.ToInt32(section.section_code) == attendance.SectionCode)
                        {
                            section.attendanceTaken = "&nbsp;&nbsp;***Attendance Not Taken***";
                        }
                    }
                }
            }


            // Get the Parent's emails using the students EA7RecordsId
            //List<GetFacultyClassAttendanceTakenById> facultyList = db.GetFacultyClassAttendanceTakenById(Convert.ToInt32(currentUser.school_uid)).ToList();

            //List<GetFacultyClassAttendanceTaken_Result> facultyList = db.GetFacultyClassAttendanceTakenById(Convert.ToInt32(2591)).ToList();

            //List<GetFacultyClassAttendanceTaken_Result> facultyList = db.GetFacultyClassAttendanceTaken().ToList();

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //return View(_filterCourseList);
            return View(filterCourseList);
        }


        /// <summary>
        /// The Section view for each student in the Course Section
        /// </summary>
        /// <param name="id">Section Id.</param>
        /// <param name="currentDate">Current Date selected from the JQuery Datepicker.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Teacher, Front Desk")]
        public ActionResult Section(long id, DateTime currentDate)
        {

            List<Course> totalCourseList;

            totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            SetGridMVCViewBag();

            // This is to check if the section meets certian business rules. See the DebugController and view. It is enable there.
            // This is commentted out becuase of performance reasons. 
            //string sectionValidationSummary = SectionService.Instance.ValidateSection(id);
            //if (!string.IsNullOrEmpty(sectionValidationSummary))
            //    return RedirectToAction("Error", new { errorMessage = sectionValidationSummary, actionName = "Index" });

            if(totalCourseList == null || totalCourseList.Count == 0)
            {
                log.ErrorFormat("[ERROR] - [CourseController - Section] - totalCourseList is null or 0.");
            }

            //Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Course course = totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Section section = course.section.Single(x => x.id == id);

            // We only want to display the class attendance on the days that the class is scheduled.
            // So we need to find out when the class is scheduled based on days and set the current date in the
            // datepicker to the closest day. The datepicker will take care of the rest by disabling the days
            // that the class does not meet.

            // The currentDate is set the the current date when this page is first accessed.
            // The user can change the date and will post back to this method.

            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday
            // Disable the JQuery Datepicker days the class is not scheduled on.

            //string daysToDisable = ClassSchedule.Instance.ClassDaysToDisable(section.description);
            //string daysToDisable = ClassSchedule.Instance.ClassDaysToDisable(section.section_title);

            List<string> weekdays = new List<string>() { "0", "1", "2", "3", "4", "5", "6" };

            // 12/2/2014 KDC - We need to disable this becuase it doesn't work for modified days.
            //List<string> daysToDisableList = weekdays.Except(section.meeting_days).ToList();

            // 12/2/2014 KDC - Enable all the days to work for modified days.
            List<string> daysToDisableList = new List<string>() { "0", "6" };

            string daysToDisable = string.Join(",", daysToDisableList);

            ViewBag.daysToDisable = daysToDisable;

            // Check the date to when the class meets. If the date is on a disabled date, then find the next date the class is schedule on.
            // This will only happen the first time this page is accessed.
            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday

            int dayOfWeekNumber = (int)currentDate.DayOfWeek;
            int nextDayOfWeekNumber;

            if (daysToDisable.Contains(dayOfWeekNumber.ToString()))
            {
                // ++dayOfWeekNumber needs to be incremented before next line.
                nextDayOfWeekNumber = (dayOfWeekNumber == 5) ? dayOfWeekNumber = 1 : ++dayOfWeekNumber;
                currentDate = ClassSchedule.GetNextDateForDay(DateTime.Now, (DayOfWeek)Enum.Parse(typeof(DayOfWeek), nextDayOfWeekNumber.ToString()));
            }

            string searchDate = currentDate.ToString("yyyy-MM-dd");

            ViewBag.searchDate = searchDate;

            // Get the attendance codes
            ViewBag.AttendanceCode = AttendanceService.Instance.GetAttendanceCodes();

            // API Call
            // List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsApi(section.id, false);

            // Json File Call
            // For increase proformance
            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, true);

            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            // Get the teachers. Used for roster printing.
            long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
            section.teachers = enrollmentList.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

            // Get the students
            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            section.enrollments = enrollmentList;

            // Get the section attendance records
            //List<Schoology.Api.Entities.Models.Attendance> attendanceList = SectionService.Instance.GetSectionAttendanceByDay(section.id, searchDate);
            List<Schoology.Api.Entities.Models.Attendance> attendanceList = AttendanceService.Instance.GetAttendanceByDate(section.id, searchDate);

            foreach(Enrollment enrollment in section.enrollments)
            {

                List<Schoology.Api.Entities.Models.Attendance> list = attendanceList.Where(x => x.enrollment_id == enrollment.id).ToList();

                List<Schoology.Api.Entities.Models.Attendance> attendanceListModified = new List<Api.Entities.Models.Attendance>();

                if (list.Count > 0)
                {
                    // There should be only 1 record
                    Schoology.Api.Entities.Models.Attendance attendanceModel = new Api.Entities.Models.Attendance();
                    attendanceModel = list[0];

                    try
                    {
                        int blackBaudCode = 0;
                        int schoologyStatusCode = 0;


                        // Separate the attendance code.
                        if (attendanceModel.status == 1)
                        {
                            attendanceModel.comment = attendanceModel.comment;
                            attendanceModel.status = 11;
                        }
                        else
                        {
                            blackBaudCode = Convert.ToInt16(attendanceModel.comment.Substring(0, 1)); // or comment[0]
                            schoologyStatusCode = attendanceModel.status;
                            attendanceModel.comment = attendanceModel.comment.Substring(1);
                            attendanceModel.status = (schoologyStatusCode * 10) + blackBaudCode;
                        }



                    }
                    catch (Exception ex)
                    {
                        attendanceModel.comment = "";
                        attendanceModel.status = -1;
                    }

                    attendanceListModified.Add(attendanceModel);

                    enrollment.attendances = attendanceListModified;
                }
                else
                {
                    List<Schoology.Api.Entities.Models.Attendance> empyList = new List<Schoology.Api.Entities.Models.Attendance>();

                    Schoology.Api.Entities.Models.Attendance attendance = new Schoology.Api.Entities.Models.Attendance();

                    // Initialize the attendance record
                    attendance.enrollment_id = enrollment.id;

                    attendance.comment = ""; 

                    attendance.status = -1;
                    attendance.date = searchDate;

                    empyList.Add(attendance);

                    enrollment.attendances = empyList;
                }
            }

            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);

        }

        /// <summary>
        /// Update the Schoology Student Attendance using the API
        /// </summary>
        /// <param name="id">Section Id</param>
        /// <param name="attendancedate">The date for the attendance.</param>
        /// <param name="section">Section record from view.</param>
        /// <param name="fc">Form Collection controls.</param>
        /// <param name="sid">Array of Student Enrollment Ids from checkboxes.</param>
        /// <param name="checkall">Check All checkbox.</param>
        /// <returns></returns>
        [Authorize(Roles = "Admin, School Admin, Teacher, Front Desk")]
        [HttpPost]
        public ActionResult Section(long id, string attendancedate, Section section, FormCollection fc, string[] sid, bool checkall)
        {

            SetGridMVCViewBag();

            int selectAllAttendanceCode = !string.IsNullOrEmpty(fc["SelectAllAttendanceCodes"]) ? Convert.ToInt32(fc["SelectAllAttendanceCodes"]) : 0;

            string selectAllComment = !string.IsNullOrEmpty(fc["SelectAllComment"]) ? fc["SelectAllComment"] : "";

            // API
            // https://api.schoology.com/v1/sections/273174388/attendance
            // Put json syntax. The documentation if currently incorrect.

            //{
            //    "attendances": {
            //        "attendance": [
            //            {
            //                "enrollment_id": 1300195944,
            //                "date": "2015-06-02",
            //                "status": 4,
            //                "comment": "Sports"
            //            },
            //                        {
            //                "enrollment_id": 1300195944,
            //                "date": "2015-06-03",
            //                "status": 3,
            //                "comment": "Sick"
            //            }
            //        ]
            //    }
            //}


            // Save the attendance records (create or update)
            List<Schoology.Api.Entities.Models.Attendance> list = new List<Schoology.Api.Entities.Models.Attendance>();

            List<Enrollment> filterEnrollment;

            // If Global select do this
            if (checkall)
            {
                // Convert the checkboxes that are checked to a list of enrollment ids.
                List<string> list2 = sid.ToList();
                // Filter out the section enrollements from the ones that were checked.
                filterEnrollment = section.enrollments.FindAll(emp => list2.Contains(emp.id.ToString()));

            }
            else
            {
                filterEnrollment = section.enrollments;
            }

            if (filterEnrollment.Count > 0)
            {
                // Select only records that have status of:
                //  1: present
                //  2: absent
                //  3: late
                //  4: excused
                foreach (Enrollment enrollment in filterEnrollment)
                {
                    //List<Schoology.Api.Entities.Models.Attendance> attendances = enrollment.attendances;

                   // Schoology.Api.Entities.Models.Attendance attendance = attendances[0];
                    Schoology.Api.Entities.Models.Attendance attendanceModel = new Schoology.Api.Entities.Models.Attendance();
                    attendanceModel = enrollment.attendances[0];

                    int blackBaudCode = 0;
                    int schoologyStatusCode = 0;

                    if (checkall)
                    {
                        // Separate the attendance code.
                        blackBaudCode = selectAllAttendanceCode % 10;
                        schoologyStatusCode = selectAllAttendanceCode / 10;
                        attendanceModel.comment = selectAllComment;
                        // old code
                        //enrollment.attendances[0].status = selectAllAttendanceCode;
                        //enrollment.attendances[0].comment = selectAllComment;
                        //list.Add(enrollment.attendances[0]);


                        // Set the schoology attendance code (1, 2, 3, 4)
                        attendanceModel.status = schoologyStatusCode;

                        // BUG: To fix the date problem.
                        if (attendanceModel.status == 1)
                        {

                            if (string.IsNullOrEmpty(attendanceModel.comment))
                            {
                                attendanceModel.comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                            }

                        }
                        else
                        {
                            if (string.IsNullOrEmpty(attendanceModel.comment))
                            {
                                attendanceModel.comment = " ";
                            }

                            attendanceModel.comment = blackBaudCode.ToString() + attendanceModel.comment;
                        }

                        list.Add(attendanceModel);

                    }
                    else if (enrollment.attendances[0].status > 0)
                    {
                        // Separate the attendance code.
                        blackBaudCode = attendanceModel.status % 10;
                        schoologyStatusCode = attendanceModel.status / 10;


                        // Set the schoology attendance code (1, 2, 3, 4)
                        attendanceModel.status = schoologyStatusCode;

                        // BUG: To fix the date problem.
                        if (attendanceModel.status == 1)
                        {
                            if (string.IsNullOrEmpty(attendanceModel.comment))
                            {
                                attendanceModel.comment = " "; // This because we don't want the record to show up the the students overall attendance summary.
                            }

                        }
                        else
                        {
                            if (string.IsNullOrEmpty(attendanceModel.comment))
                            {
                                attendanceModel.comment = " ";
                            }
                            attendanceModel.comment = blackBaudCode.ToString() + attendanceModel.comment;
                        }
                        list.Add(attendanceModel);
                    }

                }

                // Update the attendance using the Schoology API 
                AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                attendanceApiJson.attendances = new Attendances() { attendance = list };

                if (attendanceApiJson.attendances.attendance.Count > 0)
                {
                    string returnvalue = SectionService.Instance.PutSectionAttendance(section.id, attendanceApiJson);
                }

                // Get the user id (Blackbaud ea7recordsid) 
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                List<Claim> claims = identity.Claims.ToList();
                var claimUid = claims.Where(c => c.Type == "http://www.schoology.com/uid").Select(c => c.Value).SingleOrDefault();
                long uid = Convert.ToInt64(claimUid);

                User user = UserService.Instance.GetUser(uid);
                int facultyID = Convert.ToInt32(user.school_uid);

                // Get the section (Blackbaud ea7classid)
                Section currentSection = SectionService.Instance.GetSection(id);
                int sectionCode = Convert.ToInt32(currentSection.section_code);

                // Convert the string date to datetime
                DateTime attendanceDate = DateTime.Parse(attendancedate);

                // Search to see if the record has already been createed. If not, create the record. If so, bypass saving the record
                List<Custom.Entities.Models.Attendance> attendanceList = db.Attendance.Where(p => p.SectionCode == sectionCode && 
                                                                                                  p.FacultyID == facultyID && 
                                                                                                  p.AttendanceDate == attendanceDate).ToList();

                if (attendanceList.Count == 0)
                {
                    Custom.Entities.Models.Attendance attendance = new Custom.Entities.Models.Attendance();
                    attendance.SectionCode = sectionCode;
                    attendance.FacultyID = facultyID;
                    attendance.AttendanceDate = DateTime.Parse(attendancedate);
                    attendance.DateCreated = DateTime.Now;
                    try
                    {
                        db.Attendance.Add(attendance);
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        log.ErrorFormat("[Faculty Attendance] - CourseController - SectionCode {0} - FacultyID {1} - AttendanceDate {2} - DateCreated {3}", attendance.SectionCode, attendance.FacultyID, attendance.AttendanceDate, attendance.DateCreated);
                    }
                }
 

            }
            return RedirectToAction("Section", new {id = id, currentDate = attendancedate});

            //return Json(new { RedirectUrl = Url.Action("Section", new { id = id, currentDate = attendancedate }) });

        }

        /// <summary>
        /// Set the navigation variables.
        /// </summary>
        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }
        /// <summary>
        /// Displays the Error message. This is if we used the Section Validation routine.
        /// </summary>
        /// <param name="errorMessage"></param>
        /// <param name="actionName"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        public ActionResult Error(string errorMessage, string actionName)
        {
            SetGridMVCViewBag();

            ViewBag.ErrorMessage = errorMessage;
            ViewBag.ActionName = actionName;
            return View();
        }

        [Authorize(Roles = "Admin, School Admin, Teacher, Front Desk")]
        public ActionResult RosterPDF(long id, DateTime date)
        {
            //KDC added
            List<Course> totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            ViewBag.Date = date.ToShortDateString();

            //Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Course course = totalCourseList.Single(p => p.section.Any(x => x.id == id));
            Section section = course.section.Single(x => x.id == id);

            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, true);

            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            // Get the teachers. Used for roster printing.
            long superAdminId = Convert.ToInt64(ConfigurationManager.AppSettings["superAdmin"]);
            section.teachers = enrollmentList.Where(x => x.admin == 1 && x.status == "1" && x.uid != superAdminId).ToList();

            // Get the students
            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            section.enrollments = enrollmentList;

            return new PdfActionResult("RosterPDF", section);

        }

    }

}

