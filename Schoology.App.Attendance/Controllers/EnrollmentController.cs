﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Configuration;
using Schoology.Api.Service;
using Schoology.Api.Entities.Models;
using System.Net;
using Schoology.App.Attendance.Models;
using log4net;
using Schoology.Api.Entities;


using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Web.Routing;
namespace Schoology.App.Attendance.Controllers
{
    public class EnrollmentController : Controller
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static List<User> _totalUserList { get; set; }

        private List<User> LoadStudents(string searchString)
        {
            // Create the list to hold the users.
            List<User> filterUserList = new List<User>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {
                // Get the users from Schoology Json File instead of calling the Schoology API directly.
                _totalUserList = UserService.Instance.GetUsersJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }


            }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    filterUserList = _totalUserList.Where(s => s.name_last.ToLower().Contains(searchString.ToLower())
                        || s.name_first.ToLower().Contains(searchString.ToLower())).ToList();
                }
            }
            else
            {
                filterUserList = _totalUserList;
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //List<Attendance> list = attendanceRoot.attendance.Where(x => x.date == "2015-06-03").ToList();

            return filterUserList;
        }


///        [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult Index(string searchString)
        {
            return View(LoadStudents(searchString));
        }

      

       
   ///     [Authorize(Roles = "Admin, School Admin, Front Desk")]
        public ActionResult Update(long id)
        {

            SetGridMVCViewBag();

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            User user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);

        }

  ///      [Authorize(Roles = "Admin, School Admin, Front Desk")]
        [HttpPost]
        public ActionResult Update(long id, User user)
        {
            SetGridMVCViewBag();

            int ea7recordsId = 0;

            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            user = _totalUserList.Single(x => x.id == id);

            if (user == null)
            {
                return HttpNotFound();
            }

            int n;
            bool isNumeric = int.TryParse(user.school_uid, out n);
            if (isNumeric)
            {

                // Get Blackbaud EA7RecordsId
                ea7recordsId = Convert.ToInt16(user.school_uid);

                // Get the dictionary mapping of Blackbaud class id (key) to Schoology section id (value)
                Dictionary<long, long> blackBaudSchoologySectionIds = CourseService.Instance.GetBlackBaudSchoologySectionsIds();


                // NOTE THESE ARE THE BLACKBAUD CLASS ID'S. WE WILL GET THE SCHOOLOGY SECTION IDS FROM THE MAPPING WHEN WE DELETE AND ADD.
                // Get the users sections Id's
                List<long> schoologyList = user.sections.Select(x => Convert.ToInt64(x.section_code)).ToList();

                // TODO:
                // Change this to get the sections from Blackbaud
                // Call the store procedure [Schoology.GetStudentClasses] ea7recordsid

                List<GetStudentClasses_Result> studentClasses = StudentRepository.GetStudentClasses(ea7recordsId);
                List<long> blackBaudList = studentClasses.Select(x => Convert.ToInt64(x.ClassId)).ToList();

                // Get the difference section id's to add
                List<long> classesToAddList = blackBaudList.Except(schoologyList.Select(x => x)).ToList();

                // Get the difference sections id's to delete
                List<long> classesToDeleteList = schoologyList.Except(blackBaudList.Select(x => x)).ToList();



                //*****************************
                // Delete the section classes
                //*****************************

                if (classesToDeleteList.Count > 0)
                {
                    Dictionary<long, long> userSectionsEnrollmentIdList = user.sectionsEnrollmentIds;

                    long enrollmentId = 0;
                    foreach (long blackBaudClassId in classesToDeleteList)
                    {
                        // GET THE SCHOOLOGY SECTION ID FROM THE MAPPING.
                        if (blackBaudSchoologySectionIds.ContainsKey(blackBaudClassId))
                        {
                            long schoologySectionId = blackBaudSchoologySectionIds[blackBaudClassId];

                            if (user.sectionsEnrollmentIds.ContainsKey(schoologySectionId))
                            {
                                enrollmentId = user.sectionsEnrollmentIds[schoologySectionId];

                                log.InfoFormat("[Sync Enrollment] [DELETE] - User: {0} - {1} - Section Info: SectionId - {2} - EnrollmentId - {3}",
                                    user.uid, user.name_first + " " + user.name_last, schoologySectionId.ToString(), enrollmentId);


                                string result = EnrollmentService.Instance.DeleteUserEnrollment(schoologySectionId.ToString(), enrollmentId);
                            }

                        }


                    }
                }
                //***********************
                // Add the new sections
                //***********************
                if (classesToAddList.Count > 0)
                {
                    foreach (long blackBaudClassId in classesToAddList)
                    {
                        // GET THE SCHOOLOGY SECTION ID FROM THE MAPPING.
                        if (blackBaudSchoologySectionIds.ContainsKey(blackBaudClassId))
                        {
                            long schoologySectionId = blackBaudSchoologySectionIds[blackBaudClassId];

                            log.InfoFormat("[Sync Enrollment] [ADD] - User: {0} - {1} - Section Info: SectionId - {2}",
                                user.uid, user.name_first + " " + user.name_last, schoologySectionId.ToString());


                            string result = EnrollmentService.Instance.PostUserEnrollment(id.ToString(), schoologySectionId);

                        }
                    }
                }
            }
            else
            {
                log.ErrorFormat("[Sync Enrollment] [INVALID USER] - User: {0} - {1}", user.uid, user.name_first + " " + user.name_last);
            }


            ViewBag.Message = "User class enrollments have been update.";


            return RedirectToAction("Index", new RouteValueDictionary {
                                    { "grid-page", @ViewBag.gridPage },
                                    { "grid-filter", @ViewBag.gridFilter },
                                    { "grid-column", @ViewBag.gridColumn },
                                    { "grid-dir", @ViewBag.gridDir },
                                    { "SearchString", @ViewBag.SearchString }
                                 });

        }

        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }

    }
}