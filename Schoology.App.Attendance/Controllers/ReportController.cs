﻿using log4net;
using Schoology.Api.Entities.Models;
using Schoology.Api.Service;
using Schoology.App.Attendance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Schoology.App.Attendance.Controllers
{
    public class ReportController : Controller
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="currentDate"></param>
        /// <returns></returns>
    ///    [Authorize(Roles = "Admin, Teacher")]
        public ActionResult FacultyClassAttendance(DateTime currentDate)
        {
            List<Course> totalCourseList;
            List<Course> filterCourseList;

            string searchDate = currentDate.ToString("yyyy-MM-dd");

            ViewBag.searchDate = searchDate;

            totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();
            filterCourseList = totalCourseList;

            // If this is a teacher, only get the courses the teacher teaches.
            if (User.IsInRole("Teacher") || User.IsInRole("Admin"))
            {

                // TODO: Make this more easier to use
                // Get the current schoology user id.
                ClaimsIdentity identity = (ClaimsIdentity)User.Identity;
                List<Claim> claims = identity.Claims.ToList();
                var claimUid = claims.Where(c => c.Type == "http://www.schoology.com/uid").Select(c => c.Value).SingleOrDefault();
                long uid = Convert.ToInt64(claimUid);

                //log.ErrorFormat("[Course] - IsInRole(Teacher) - {0} - ID - {1}", User.Identity.Name, uid);

                // Only get the courses the teacher teaches
                //List<Course> teachersList = _totalCourseList.Where(x => x.section.Any(o => o.enrollments.Any(w => w.uid == uid))).ToList();
                List<Course> teachersList = totalCourseList.Where(x => x.section.Any(o => o.enrollments.Any(w => w.uid == uid))).ToList();

                // Now get only the sections the teacher teaches.
                foreach (Course course in teachersList)
                {
                    List<Section> sectionList = course.section.Where(x => x.enrollments.Any(e => e.uid == uid)).ToList();
                    course.section = sectionList;
                }

                //_totalCourseList = teachersList;
                totalCourseList = teachersList;

                // Maybe read this from app.config incase the report period changes
                // Note: this would have to change in the email template also.
                // This report is to run on Mondays and report on the previous week (Monday - Friday)
                DateTime date = currentDate;

                // DEBBUG
                //DateTime date = Convert.ToDateTime("9/25/2015");

                List<AttendanceModel> attendanceModelList = new List<AttendanceModel>();

                // For the given date, get a list of all the UA's for the section and put it in a list.
                foreach (Course course in totalCourseList)
                {
                    foreach (Section section in course.section)
                    {
                        // Get the attendance for the section for the date. Note: The API needs the date in yyyy-MM-dd.
                        List<Schoology.Api.Entities.Models.Attendance> attendanceList = AttendanceService.Instance.GetAttendanceByDate(section.id, date.ToString("yyyy-MM-dd"));

                        // Filter out the Schoology status of 2
                        //attendanceList = attendanceList.Where(x => x.status == 4).ToList();

                        // Filter out the UA from the comment number is the first position
                        //{"11", "Present"},
                        //{"43", "V"},
                        //{"22", "TA"},
                        //{"21", "UA"},
                        //{"41", "RA"},
                        //{"42", "S"},
                        //{"31", "T"},
                        //{"23", "DA"},
                        //{"32", "DR"},
                        foreach (Schoology.Api.Entities.Models.Attendance attendance in attendanceList)
                        {
                            //if (attendance.comment.Substring(0, 1) == "3")
                            //{
                                // Get the User
                                // Get the user sections. Calling the API directly
                                section.enrollments = EnrollmentService.Instance.GetSectionEnrollments(section.id, false, false);

                                if (section.enrollments.Count > 0)
                                {
                                    section.enrollments = section.enrollments.Where(e => e.id == attendance.enrollment_id).ToList();

                                    // If the count is zero, that means a user is a teacher that is enrolled in the section
                                    // and is not enrolled as a student.
                                    if (section.enrollments.Count > 0)
                                    {
                                        Enrollment enrollment = section.enrollments[0];

                                        List<AttendanceModel> foundList = attendanceModelList.Where(x => x.UID == enrollment.uid.ToString()).ToList();
                                        if (foundList.Count > 0)
                                        {

                                            // Create the section info
                                            AttendanceStudentSectionModel attendanceStudentSectionModel = new AttendanceStudentSectionModel();
                                            attendanceStudentSectionModel.CourseName = section.course_title;
                                            attendanceStudentSectionModel.CourseCode = section.course_code;
                                            attendanceStudentSectionModel.SectionTitle = section.section_title;
                                            attendanceStudentSectionModel.SectionCode = section.section_code;
                                            attendanceStudentSectionModel.BlockColor = ClassSchedule.Instance.BlockColor(section.description);

                                            attendanceStudentSectionModel.Date = attendance.date;

                                            string attendanceCode = attendance.status.ToString() + attendance.comment.Substring(0, 1);
                                            attendanceStudentSectionModel.AttendanceCode = attendance.status;
                                            attendanceStudentSectionModel.AttendanceCodeTitle = AttendanceService.Instance.GetAttendanceCode(attendanceCode);
                                            attendanceStudentSectionModel.AttendanceComment = attendance.comment.Substring(1);

                                            // Add the section
                                            foundList[0].AttendanceStudentSectionModel.Add(attendanceStudentSectionModel);
                                        }
                                        else
                                        {
                                            AttendanceModel attendanceModel = new AttendanceModel();
                                            attendanceModel.SchoolUID = Convert.ToInt32(enrollment.school_uid); // This is Blackbaud's EA7RecordsId, which is used to look up the parents emails.
                                            attendanceModel.UID = enrollment.uid.ToString();
                                            attendanceModel.Title = enrollment.name_title;
                                            attendanceModel.FirstName = enrollment.name_first;
                                            attendanceModel.LastName = enrollment.name_last;
                                            attendanceModel.Date = date;

                                            attendanceModelList.Add(attendanceModel);


                                            // Initialize the list
                                            attendanceModel.AttendanceStudentSectionModel = new List<AttendanceStudentSectionModel>();


                                            // Create the section info
                                            AttendanceStudentSectionModel attendanceStudentSectionModel = new AttendanceStudentSectionModel();
                                            attendanceStudentSectionModel.CourseName = section.course_title;
                                            attendanceStudentSectionModel.CourseCode = section.course_code;
                                            attendanceStudentSectionModel.SectionTitle = section.section_title;
                                            attendanceStudentSectionModel.SectionCode = section.section_code;
                                            attendanceStudentSectionModel.BlockColor = ClassSchedule.Instance.BlockColor(section.description);

                                            attendanceStudentSectionModel.Date = attendance.date;

                                            string attendanceCode = attendance.status.ToString() + attendance.comment.Substring(0, 1);
                                            attendanceStudentSectionModel.AttendanceCode = attendance.status;
                                            attendanceStudentSectionModel.AttendanceCodeTitle = AttendanceService.Instance.GetAttendanceCode(attendanceCode);
                                            attendanceStudentSectionModel.AttendanceComment = attendance.comment.Substring(1);

                                            // Add the section
                                            attendanceModel.AttendanceStudentSectionModel.Add(attendanceStudentSectionModel);

                                        }

                                    }
                                }
                            //}

                        }
                    }
                }


                AttendanceReportModel attendanceReportModel = new AttendanceReportModel();
                attendanceReportModel.Date = currentDate.ToShortDateString();
                attendanceReportModel.AttendanceModelList = attendanceModelList;

                return View(attendanceReportModel);
            }



            return View();
        }
    }



    

}