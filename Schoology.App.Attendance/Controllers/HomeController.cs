﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using ColorCode;
using System.Web.Security;
using Schoology.Api.Saml;
using Schoology.Api.Service;
using System.Security.Claims;

namespace Schoology.App.Attendance.Controllers
{
    public class HomeController : Controller
    {
        //[Authorize(Roles = "Admin, School Admin, Front Desk, Teacher")]
        public ActionResult Index()
        {
            return View();
        }

    }
}