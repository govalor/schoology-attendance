﻿using Newtonsoft.Json;
using Schoology.Api.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Schoology.Api.Entities.Models;
using System.Configuration;
using Schoology.App.Attendance.Models;
using System.Net;

namespace Schoology.App.Attendance.Controllers
{
    public class DebugController : Controller
    {
        private static List<GradingPeriod> _gradingPeriodList = new List<GradingPeriod>();
        private static List<Course> _totalCourseList { get; set; }
        private static List<Course> _filterCourseList { get; set; }

        // GET: Course
        public ActionResult Index(string searchString)
        {

            List<BlockClassSchedule> classScheduleList = ClassSchedule.Instance.Schedule;

            List<BlockClassSchedule> mondayList = classScheduleList.Where(x => x.Day == "Monday").ToList();
            List<BlockClassSchedule> tuesdayList = classScheduleList.Where(x => x.Day == "Tuesday").ToList();
            List<BlockClassSchedule> wednesdayList = classScheduleList.Where(x => x.Day == "Wednesday").ToList();
            List<BlockClassSchedule> thrusdayList = classScheduleList.Where(x => x.Day == "Thursday").ToList();
            List<BlockClassSchedule> fridayList = classScheduleList.Where(x => x.Day == "Friday").ToList();


            // GradingPeriod dropdown code
            // Add method param int? gradingPeriod
            //// Firsttime load the grading periods
            //if(gradingPeriod == null)
            //{
            //    // Get the Grading Periods from Schoology
            //    _gradingPeriodList = CourseService.Instance.GetGradingPeriods();
            //    gradingPeriod = CourseService.Instance.GetCurrentGradingPeriod(_gradingPeriodList);
            //}

            //// Populate the Grading Period dropdown.
            //ViewBag.gradingPeriod = new SelectList(_gradingPeriodList, "id", "title", gradingPeriod);
            //int gradingPeriodId = gradingPeriod.GetValueOrDefault();


            // Create the list to hold the users.
            List<Course> filterList = new List<Course>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {
                // Get the users from Schoology
                _totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }


            }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    _filterCourseList = _totalCourseList.Where(s =>

                        s.title.ToLower().Contains(searchString.ToLower()) ||
                        s.course_code.ToLower().Contains(searchString.ToLower()) ||
                        s.section.Any(c => c.section_title.ToLower().Contains(searchString.ToLower()))

                        ).ToList();

                }
            }
            else
            {
                _filterCourseList = _totalCourseList;
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //List<Course> courseList = CourseService.Instance.GetCourses();

            //List<Section> sectionList = SectionService.Instance.GetCourseSections(64970329, gradingPeriodId);

            //List<Course> courseSectionList = CourseService.Instance.GetCoursesWithSections();


            return View(_filterCourseList);
        }

        public ActionResult Course(string searchString)
        {

            List<BlockClassSchedule> classScheduleList = ClassSchedule.Instance.Schedule;

            List<BlockClassSchedule> mondayList = classScheduleList.Where(x => x.Day == "Monday").ToList();
            List<BlockClassSchedule> tuesdayList = classScheduleList.Where(x => x.Day == "Tuesday").ToList();
            List<BlockClassSchedule> wednesdayList = classScheduleList.Where(x => x.Day == "Wednesday").ToList();
            List<BlockClassSchedule> thrusdayList = classScheduleList.Where(x => x.Day == "Thursday").ToList();
            List<BlockClassSchedule> fridayList = classScheduleList.Where(x => x.Day == "Friday").ToList();


            // GradingPeriod dropdown code
            // Add method param int? gradingPeriod
            //// Firsttime load the grading periods
            //if(gradingPeriod == null)
            //{
            //    // Get the Grading Periods from Schoology
            //    _gradingPeriodList = CourseService.Instance.GetGradingPeriods();
            //    gradingPeriod = CourseService.Instance.GetCurrentGradingPeriod(_gradingPeriodList);
            //}

            //// Populate the Grading Period dropdown.
            //ViewBag.gradingPeriod = new SelectList(_gradingPeriodList, "id", "title", gradingPeriod);
            //int gradingPeriodId = gradingPeriod.GetValueOrDefault();


            // Create the list to hold the users.
            List<Course> filterList = new List<Course>();

            // Initialize the Grid.MVC searching, sorting and search string.
            GridMVCModel gridMVCModel = new GridMVCModel();

            ViewBag.gridPage = 1;
            ViewBag.gridFilter = "";
            ViewBag.gridColumn = "";
            ViewBag.gridDir = 0;
            ViewBag.SearchString = "";

            // Proformance Issues
            // We only want to load data when we hit this page.
            // If the post-back for grid paging and filtering, do not reload the data.

            // If no query strings, load the data once and store in static variable
            if (Request.QueryString.Count == 0)
            {
                // Get the users from Schoology
                _totalCourseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

                // Set the Grid.MVC grid-page to 1 because if we go to the details page and return it will reload all the data.
                ViewBag.gridPage = 1;

            }
            else
            {
                // Set the view bag variables to be passed to the details page so we can return back to where we started from.
                // Grid Page
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-page"]))
                {
                    ViewBag.gridPage = int.Parse(Request.QueryString["grid-page"]);
                }

                // Grid Filter
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-filter"]))
                {
                    ViewBag.gridFilter = Request.QueryString["grid-filter"];
                }

                // Grid Sort Direction
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-dir"]))
                {
                    ViewBag.gridDir = Request.QueryString["grid-dir"];
                }

                // Grid Sort Column
                if (!string.IsNullOrWhiteSpace(Request.QueryString["grid-column"]))
                {
                    ViewBag.gridColumn = Request.QueryString["grid-column"];
                }

                // Filter Textbox
                if (!string.IsNullOrWhiteSpace(Request.QueryString["SearchString"]))
                {
                    ViewBag.SearchString = Request.QueryString["SearchString"];
                }


            }

            // Filter the list by FirstName or LastName.
            if (!string.IsNullOrEmpty(searchString))
            {
                ViewBag.SearchString = searchString;

                // Now that we have all the users, check to see if we need to filter them.
                if (!string.IsNullOrEmpty(searchString))
                {
                    _filterCourseList = _totalCourseList.Where(s =>

                        s.title.ToLower().Contains(searchString.ToLower()) ||
                        s.course_code.ToLower().Contains(searchString.ToLower()) ||
                        s.section.Any(c => c.section_title.ToLower().Contains(searchString.ToLower()))

                        ).ToList();

                }
            }
            else
            {
                _filterCourseList = _totalCourseList;
            }

            gridMVCModel.GridPage = Convert.ToInt32(ViewBag.gridPage);
            gridMVCModel.GridFilter = ViewBag.gridFilter;
            gridMVCModel.GridColumn = ViewBag.gridColumn;
            gridMVCModel.GridDir = Convert.ToInt32(ViewBag.gridDir);
            gridMVCModel.SearchString = ViewBag.searchString;

            Session["gridMVCModel"] = gridMVCModel;

            //List<Course> courseList = CourseService.Instance.GetCourses();

            //List<Section> sectionList = SectionService.Instance.GetCourseSections(64970329, gradingPeriodId);

            //List<Course> courseSectionList = CourseService.Instance.GetCoursesWithSections();


            return View(_filterCourseList);
        }

        public ActionResult Section(long id, DateTime currentDate)
        {

            SetGridMVCViewBag();

            string sectionValidationSummary = SectionService.Instance.ValidateSection(id);
            if (!string.IsNullOrEmpty(sectionValidationSummary))
                return RedirectToAction("Error", new { errorMessage = sectionValidationSummary, actionName = "Index" });

            Course course = _totalCourseList.Single(p => p.section.Any(x => x.id == id));

            Section section = course.section.Single(x => x.id == id);

            // We only want to display the class attendance on the days that the class is scheduled.
            // So we need to find out when the class is scheduled based on days and set the current date in the
            // datepicker to the closest day. The datepicker will take care of the rest by disabling the days
            // that the class does not meet.

            // The currentDate is set the the current date when this page is first accessed.
            // The user can change the date and will post back to this method.

            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday
            // Disable the JQuery Datepicker days the class is not scheduled on.


            //string daysToDisable = ClassSchedule.Instance.ClassDaysToDisable(section.description);
            string daysToDisable = ClassSchedule.Instance.ClassDaysToDisable(section.section_title);
            ViewBag.daysToDisable = daysToDisable;

            // Check the date to when the class meets. If the date is on a disabled date, then find the next date the class is schedule on.
            // This will only happen the first time this page is accessed.
            // 0 = sunday, 1 = monday, 2 = tuesday, 3 = wednesday, 4 = thrusday, 5 = friday, 6 = saturday

            int dayOfWeekNumber = (int)currentDate.DayOfWeek;
            int nextDayOfWeekNumber;
            //DateTime date = currentDate;

            if (daysToDisable.Contains(dayOfWeekNumber.ToString()))
            {
                // ++dayOfWeekNumber needs to be incremented before next line.
                nextDayOfWeekNumber = (dayOfWeekNumber == 5) ? dayOfWeekNumber = 1 : ++dayOfWeekNumber;
                currentDate = ClassSchedule.GetNextDateForDay(DateTime.Now, (DayOfWeek)Enum.Parse(typeof(DayOfWeek), nextDayOfWeekNumber.ToString()));
            }


            string searchDate = currentDate.ToString("yyyy-MM-dd");

            ViewBag.searchDate = searchDate;

            ViewBag.AttendanceCode = GetAttendanceCodes();

            //SetGridMVCViewBag();

            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}


            //User user = _totalList.Where(x => x.id == id);
            //Course course = _totalCourseList.Single(x => x.id == id);

            // API Call
            // List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsApi(section.id, false);

            // Json File Call
            // For increase proformance
            List<Enrollment> enrollmentList = EnrollmentService.Instance.GetSectionEnrollmentsJsonFile(section.id, false);

            // Only select the active students and not the teacher.
            // The current status of the enrollment.
            //  1: Active
            //  2: Expired (i.e. past course)
            //  3: Invite pending
            //  4: Request Pending
            //  5: Archived (Course specific status members can be placed in before being fully unenrolled)

            enrollmentList = enrollmentList.Where(x => x.admin != 1 && x.status == "1").ToList();

            section.enrollments = enrollmentList;

            // Get the section attendance records
            //List<Schoology.Api.Entities.Models.Attendance> attendanceList = SectionService.Instance.GetSectionAttendanceByDay(section.id, searchDate);
            List<Schoology.Api.Entities.Models.Attendance> attendanceList = AttendanceService.Instance.GetAttendanceByDate(section.id, searchDate);

            foreach (Enrollment enrollment in section.enrollments)
            {

                List<Schoology.Api.Entities.Models.Attendance> list = attendanceList.Where(x => x.enrollment_id == enrollment.id).ToList();

                if (list.Count > 0)
                {
                    enrollment.attendances = list;
                }
                else
                {
                    List<Schoology.Api.Entities.Models.Attendance> empyList = new List<Schoology.Api.Entities.Models.Attendance>();

                    Schoology.Api.Entities.Models.Attendance attendance = new Schoology.Api.Entities.Models.Attendance();

                    // Initialize the attendance record
                    attendance.enrollment_id = enrollment.id;
                    attendance.comment = "";
                    attendance.status = -1;
                    attendance.date = searchDate;

                    empyList.Add(attendance);

                    enrollment.attendances = empyList;
                }
            }

            if (section == null)
            {
                return HttpNotFound();
            }
            return View(section);

        }


        [HttpPost]
        public ActionResult Section(long id, string attendancedate, Section section, FormCollection fc, string[] sid, bool checkall)
        {

            SetGridMVCViewBag();

            int selectAllAttendanceCode = !string.IsNullOrEmpty(fc["SelectAllAttendanceCodes"]) ? Convert.ToInt32(fc["SelectAllAttendanceCodes"]) : 0;

            string selectAllComment = !string.IsNullOrEmpty(fc["SelectAllComment"]) ? fc["SelectAllComment"] : "";


            //var names = formCollection.AllKeys.Where(c => c.StartsWith("checkbox")).ToList();

            //foreach (var key in formCollection.AllKeys)
            //{
            //    var value = formCollection[key];
            //    // etc.
            //}

            //foreach (var key in formCollection.Keys)
            //{
            //    var value = formCollection[key.ToString()];
            //    // etc.
            //}

            //formCollection.GetValue("").
            //var names2 = formCollection.AllKeys.Where(c => c.StartsWith("checkbox") &&
            //            formCollection.GetValue(c) != null &&
            //            formCollection.GetValue(c).AttemptedValue == "true,false");

            // API
            // https://api.schoology.com/v1/sections/273174388/attendance
            // Put json syntax

            //{
            //    "attendances": {
            //        "attendance": [
            //            {
            //                "enrollment_id": 1300195944,
            //                "date": "2015-06-02",
            //                "status": 4,
            //                "comment": "Sports"
            //            },
            //                        {
            //                "enrollment_id": 1300195944,
            //                "date": "2015-06-03",
            //                "status": 3,
            //                "comment": "Sick"
            //            }
            //        ]
            //    }
            //}


            // Save the attendance records (create or update)
            List<Schoology.Api.Entities.Models.Attendance> list = new List<Schoology.Api.Entities.Models.Attendance>();

            List<Enrollment> filterEnrollment;

            // If Global select do this
            if (checkall)
            {
                // Convert the checkboxes that are checked to a list of enrollment ids.
                List<string> list2 = sid.ToList();
                // Filter out the section enrollements from the ones that were checked.
                filterEnrollment = section.enrollments.FindAll(emp => list2.Contains(emp.id.ToString()));

            }
            else
            {
                filterEnrollment = section.enrollments;
            }

            if (filterEnrollment.Count > 0)
            {
                // Select only records that have status of:
                //  1: present
                //  2: absent
                //  3: late
                //  4: excused
                foreach (Enrollment enrollment in filterEnrollment)
                {
                    //List<Schoology.Api.Entities.Models.Attendance> attendances = enrollment.attendances;

                    // Schoology.Api.Entities.Models.Attendance attendance = attendances[0];

                    if (checkall)
                    {
                        enrollment.attendances[0].status = selectAllAttendanceCode;
                        enrollment.attendances[0].comment = selectAllComment;
                        list.Add(enrollment.attendances[0]);
                    }
                    else if (enrollment.attendances[0].status > 0)
                    {
                        list.Add(enrollment.attendances[0]);
                    }

                }

                // Update the attendance using the Schoology API 
                AttendanceApiJson attendanceApiJson = new AttendanceApiJson();
                attendanceApiJson.attendances = new Attendances() { attendance = list };

                if (attendanceApiJson.attendances.attendance.Count > 0)
                {
                    string returnvalue = SectionService.Instance.PutSectionAttendance(section.id, attendanceApiJson);
                }

                // TODO:
                // Check return value of Http 200 - Success

            }
            return RedirectToAction("Section", new { id = id, currentDate = attendancedate });

            //return Json(new { RedirectUrl = Url.Action("Section", new { id = id, currentDate = attendancedate }) });

        }


        private Dictionary<string, string> GetAttendanceCodes()
        {
            return new Dictionary<string, string>
            {
                {"1", "Present"},
                {"2", "Absent"},
                {"3", "Late"},
                {"4", "Excused"},
            };

        }

        private Dictionary<string, string> GetAttendanceReason()
        {
            return new Dictionary<string, string>
            {
                {"40", "V"},
                {"20", "TA"},
                {"41", "VA"},
                {"42", "RA"},
                {"44", "S"},
                {"30", "T"},
            };

        }

        private void SetGridMVCViewBag()
        {
            if (Session["gridMVCModel"] != null)
            {
                // Set the Sorting and Filters variables in the view bag to return back to the calling page to return back to where we started from.
                GridMVCModel gridMVCModel = Session["gridMVCModel"] as GridMVCModel;
                ViewBag.gridPage = gridMVCModel.GridPage;
                ViewBag.gridFilter = gridMVCModel.GridFilter;
                ViewBag.gridColumn = gridMVCModel.GridColumn;
                ViewBag.gridDir = gridMVCModel.GridDir;
                ViewBag.SearchString = gridMVCModel.SearchString;
            }
        }

        [ValidateInput(false)]
        public ActionResult Error(string errorMessage, string actionName)
        {
            SetGridMVCViewBag();

            ViewBag.ErrorMessage = errorMessage;
            ViewBag.ActionName = actionName;
            return View();
        }

    }
}