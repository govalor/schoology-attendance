﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Schoology.App.Attendance.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Error/
        public ActionResult AccessDenied(string errorMessage)
        {
            ViewBag.ErrorMessage = errorMessage;
            return View();
        }
    }
}