﻿using log4net;
using Newtonsoft.Json;
using Schoology.Api.Entities.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Service
{
    public class EnrollmentService
    {

        // Log file
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

                // Singleton Design Pattern
        private static readonly Lazy<EnrollmentService> lazy = new Lazy<EnrollmentService>(() => new EnrollmentService());

        public ServiceProvider ServiceProvider { get; set;  }

        public static EnrollmentService Instance
        { 
            get 
            { 
                return lazy.Value;
            } 
        }

        public string ConsumerKey {
            set { ServiceProvider.ConsumerKey = value; }
        }

        public string ConsumerSecret
        {
            set { ServiceProvider.ConsumerSecret = value; }
        }


        private EnrollmentService()
        {
            // Schoology 2-legged Oauth credentials.
            ServiceProvider = ServiceProvider.Instance;
            ServiceProvider.ConsumerKey = ConfigurationManager.AppSettings["consumerKey"];
            ServiceProvider.ConsumerSecret = ConfigurationManager.AppSettings["consumerSecret"];
        }


        public void SetCredentials(string key, string secret)
        {
            ServiceProvider.ConsumerKey = key;
            ServiceProvider.ConsumerSecret = secret;
        }

        public List<Enrollment> GetSectionEnrollments(long sectionId, bool includeAdmin, bool useApi)
        {
            if(useApi)
                return GetSectionEnrollmentsApi(sectionId, includeAdmin);
            else
                return GetSectionEnrollmentsJsonFile(sectionId, includeAdmin);

        }

        public List<Enrollment> GetSectionEnrollmentsApi(long sectionId, bool includeAdmin)
        {
            List<Enrollment> list = new List<Enrollment>();

            // Schoology API has a max start and limit of 200 to get users.
            // We need to get all of them, so we will loop through them. See code below.

            string api = string.Format("https://api.schoology.com/v1/sections/{0}/enrollments?limit=200", sectionId);

            string json = ServiceProvider.GetData(api);

            if (string.IsNullOrEmpty(json))
            {
                log.ErrorFormat("EnrollmentService -> [GetSectionEnrollmentsApi] - Json is null. The api = {0}", api);
            }
            else
            {
                EnrollmentRoot root = JsonConvert.DeserializeObject<EnrollmentRoot>(json);

                list = new List<Enrollment>();

                list = list.Concat(root.enrollment).ToList();

                // Loop through the user api and concatenate all the users so we can display, page and filter them.
                while (root.links.next != null)
                {
                    json = ServiceProvider.GetData(root.links.next);

                    root = JsonConvert.DeserializeObject<EnrollmentRoot>(json);

                    // Order the enrollments by lastname, firstname
                    list = list.Concat(root.enrollment).OrderBy(c => c.name_last).OrderBy(c => c.name_first).ToList();
                }

                // Only select the students and not the teachers.
                // A teacher can be a admin of a section and a student of a section. This is a rare exception.
                if (!includeAdmin)
                {
                    list = list.Where(x => x.admin != 1).ToList();
                }

            }

            return list;

        }

        public List<Enrollment> GetSectionEnrollmentsJsonFile(long sectionId, bool includeAdmin)
        {

            // Get active courses and sections from cache file. This is more secure than reading the json file from website from above.
            List<Course> courseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            Course course = courseList.Single(p => p.section.Any(x => x.id == sectionId));

            Section section = course.section.Single(x => x.id == sectionId);

            List<Enrollment> list = new List<Enrollment>();

            if (!includeAdmin)
            {
                list = section.enrollments.Where(x => x.admin != 1).ToList();
            }
            else
            {
                // Include admins and students.
                list = section.enrollments.ToList();
            }

            // return list  data
            return list;
        }


        public string PostSuperAdminToAllCourseSections()
        {
            string returnValue = "";

            string superAdminStr = ConfigurationManager.AppSettings["superadmin"];
            long superAdminId = Convert.ToInt64(superAdminStr);

            string json = "";

            // Get all the active courses
            List<Course> courseList = CourseService.Instance.GetCoursesWithSectionsJsonFile();

            // For each course get the sections
            foreach (Course course in courseList)
            {
                if (course.section != null)
                {
                    List<Section> sectionList = course.section;
                    if (sectionList.Count > 0)
                    {
                        foreach (Section section in sectionList)
                        {
                            if (section.enrollments != null)
                            {

                                List<Enrollment> sectionEnrollmentList = section.enrollments.Where(x => x.uid == superAdminId).ToList();
                                // Not found, add superadmin
                                if (sectionEnrollmentList.Count == 0)
                                {

                                    // for each sections, check if the superadmin already is enrolled
                                    // If not enrolled, enroll superadmin in the section

                                    EnrollmentSectionPost enrollmentSectionPost = new EnrollmentSectionPost();
                                    enrollmentSectionPost.uid = superAdminStr;
                                    enrollmentSectionPost.admin = "1";
                                    enrollmentSectionPost.status = "1";

                                    json = JsonConvert.SerializeObject(enrollmentSectionPost);

                                    string api = string.Format("https://api.schoology.com/v1/sections/{0}/enrollments", section.id);

                                    try
                                    {
                                        returnValue = ServiceProvider.PostData(api, "application/json; charset=utf-8", json);
                                    }
                                    catch(Exception ex)
                                    {
                                        log.Error("[PostSuperAdminToAllCourseSections] - Error writing to " + api);
                                    }

                                }
                            }
                        }
                    }
                }
            }

            return returnValue;
        }


        /// <summary>
        /// Add new user enrollment        
        //      POST https://api.schoology.com/v1/sections/{id}/enrollments
        //      {
        //          "uid": "2461632",
        //          "admin": "0",
        //          "status": "1"
        //      }
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public string PostUserEnrollment(string userId, long sectionId)
        {
            string returnValue = "";

            string json = "";

            EnrollmentSectionPost enrollmentSectionPost = new EnrollmentSectionPost();
            enrollmentSectionPost.uid = userId;
            enrollmentSectionPost.admin = "0";
            enrollmentSectionPost.status = "1";

            json = JsonConvert.SerializeObject(enrollmentSectionPost);

            string api = string.Format("https://api.schoology.com/v1/sections/{0}/enrollments", sectionId);

            try
            {
                returnValue = ServiceProvider.PostData(api, "application/json; charset=utf-8", json);

                // We need to wait some time in between API calls, otherwise we will get following error:
                // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000

            }
            catch (Exception ex)
            {
                log.Error("[PostUserEnrollment] - Error writing to " + api);
            }

            return returnValue;
        }

        public string DeleteUserEnrollment(string sectionId, long enrollmentId)
        {
            string returnValue = "";

            string api = string.Format("https://api.schoology.com/v1/sections/{0}/enrollments/{1}", sectionId, enrollmentId);

            try
            {
                returnValue = ServiceProvider.Instance.DeleteData(api);

                // We need to wait some time in between API calls, otherwise we will get following error:
                // WebException while reading response - The remote server returned an error: (429) Too Many Requests. - [_endPoint] - https://api.schoology.com/v1/sections/311928497
                System.Threading.Thread.Sleep(250); // 1 second. If we still get the error, increase to 2 seconds or 2000
            }
            catch (Exception ex)
            {
                log.Error("[PostSuperAdminToAllCourseSections] - Error writing to " + api);
            }

            return returnValue;
        }

    }
}
