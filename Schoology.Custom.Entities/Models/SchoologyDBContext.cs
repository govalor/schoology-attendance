﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using API_Library_2._0;


public class SchoologyDbContext : DbContext
{
    // You can add custom code to this file. Changes will not be overwritten.
    // 
    // If you want Entity Framework to drop and regenerate your database
    // automatically whenever you change your model schema, please use data migrations.
    // For more information refer to the documentation:
    // http://msdn.microsoft.com/en-us/data/jj591621.aspx

    public SchoologyDbContext()
        : base("name=SchoologyDbContext")
    {
    }

    public System.Data.Entity.DbSet<Schoology.Custom.Entities.Models.Attendance> Attendance { get; set; }

    Attendance apiGen = new Attendance();


    //public virtual ObjectResult<GetFacultyClassAttendanceTaken_Result> GetFacultyClassAttendanceTakenById(Nullable<int> facultyId)
    //{
    //    var facultyIdParameter = facultyId.HasValue ?
    //        new SqlParameter("FacultyId", facultyId) :
    //        new SqlParameter("FacultyId", typeof(int));



    //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<GetFacultyClassAttendanceTaken_Result>("Schoology.GetFacultyClassAttendanceTakenById @FacultyId", facultyIdParameter);
    //}

    public virtual List<GetFacultyClassAttendanceTaken_Result> GetFacultyClassAttendanceTakenById(Nullable<int> facultyID)
    {
        List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

        list = apiGen.getStudentGradesByID(facultyID);

        return list;
    }

    public virtual List<GetFacultyClassAttendanceTaken_Result> GetFacultyClassAttendanceTaken()
    {
        List<GetFacultyClassAttendanceTaken_Result> list = new List<GetFacultyClassAttendanceTaken_Result>();

        list = apiGen.getStudentGrades();

        return list;
    }

    //public virtual ObjectResult<GetFacultyClassAttendanceTaken_Result> GetFacultyClassAttendanceTaken()
    //{
    //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteStoreQuery<GetFacultyClassAttendanceTaken_Result>("Schoology.GetFacultyClassAttendanceTaken");
    //}

}

