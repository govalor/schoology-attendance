﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Custom.Entities.Models
{
    [Table("Attendance", Schema = "Faculty")]
    public class Attendance
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int SectionCode { get; set; }

        //[Required]
        public int FacultyID { get; set; }

        //[Required]
        [DataType(DataType.Date)]
        public DateTime AttendanceDate { get; set; }

        //[Required]
        public DateTime DateCreated { get; set; }



    }
}
