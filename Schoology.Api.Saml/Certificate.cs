﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Schoology.Api.Saml
{
    public class Certificate
    {
        public static X509Certificate2 GetCertificate(string certificateName)
        {

            string filePath = AppDomain.CurrentDomain.BaseDirectory;


            X509Certificate2 cert = new X509Certificate2(filePath + @"\bin\" + certificateName);

            return cert;

        }
    }
}
