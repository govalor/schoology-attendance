﻿using Schoology.Api.Entities.Models;
using Schoology.Api.Saml.Models;
using Schoology.Api.Service;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Schoology.Api.Saml
{
    public class SamlReponse
    {
        private string _samlString = default(String);
        private XmlDocument _samlDoc = default(XmlDocument);
        private string _certificateName = default(String);
        private string _signaturePrefix = default(String);
        private string _assertionXPath = "/samlp:Response";

        #region Constructor & Properties
        public XmlDocument SamlDocument
        {
            get
            {
                return _samlDoc;
            }
            set
            {
                _samlDoc = value;
            }
        }

        public string SamlString
        {
            get
            {
                return _samlString;
            }
            set
            {
                _samlString = value;
            }
        }

        public string Signature { get; set; }
        public XmlNamespaceManager SamlNamespaceManager
        {
            get
            {
                try
                {
                    XmlNamespaceManager samlNamespaceMgr = new XmlNamespaceManager(_samlDoc.NameTable);
                    samlNamespaceMgr.AddNamespace("samlp", @"urn:oasis:names:tc:SAML:2.0:protocol");
                    samlNamespaceMgr.AddNamespace("saml", @"urn:oasis:names:tc:SAML:2.0:assertion");
                    samlNamespaceMgr.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);

                    return samlNamespaceMgr;
                }
                catch (Exception ex) 
                { 
                    throw ex;
                }
            }
        }
        public X509Certificate2 certificate
        {
            get
            {
                return Certificate.GetCertificate(_certificateName);
            }
        }

        public SamlReponse(string samlString, string signaturePrefix, string certificateName)
        {
            _samlString = samlString;
            _signaturePrefix = signaturePrefix;
            _certificateName = certificateName;

            Load(true);
        } 
        #endregion

        #region Load
        public void Load()
        {
            Load(false);
        }

        public void Load(bool decodeBeforeLoad)
        {
            try
            {
                string token = string.Empty;
                if (decodeBeforeLoad)
                {
                    byte[] encodedBA = Convert.FromBase64String(_samlString);
                    ASCIIEncoding characterEncoding = new ASCIIEncoding();
                    token = characterEncoding.GetString(encodedBA);

                    this.SamlString = token;
                }
                else
                {
                    token = _samlString;
                }

                this.SamlDocument = new XmlDocument();
                this.SamlDocument.PreserveWhitespace = true;
                this.SamlDocument.LoadXml(token);
            }
            catch (Exception)
            { 
                throw;
            }

        } 
        #endregion

        //#region GetSignatureXmlNode & GetAssertionXmlDocument
        private XmlNode GetSignatureXmlNode()
        {
            // get the signature XML node
            return this.SamlDocument.SelectSingleNode("/samlp:Response/ds:Signature", SamlNamespaceManager);

            //return SamlDocument.DocumentElement.SelectSingleNode(_assertionXPath + "/" + _signaturePrefix + ":Signature", samlNamespaceManager);
        }

        private XmlDocument GetAssertionXmlDocument()
        {
            try
            {
                XmlDocument docAssertion = new XmlDocument();
                this.SamlDocument.PreserveWhitespace = true;
                XmlNode nodeAssertion = SamlDocument.DocumentElement.SelectSingleNode(_assertionXPath, SamlNamespaceManager);
                docAssertion.LoadXml(nodeAssertion.OuterXml);
                return docAssertion;
            }
            catch (Exception)
            {
                throw;
            }
        } 
        //#endregion

        #region ValidatingAssertionSignature
        public bool IsValid()
        {
            bool bValid = false;
            // Extract Assertion
            XmlDocument docAssertion = _samlDoc; //GetAssertionXmlDocument();

            // Validating Assertion Signature
            if (certificate == null) throw new Exception("Certificate not found!");
            try
            {
                // load a new XML document
                var assertion = new XmlDocument { PreserveWhitespace = true };
                assertion.LoadXml(_samlString);

                // get the signature XML node
                //var signNode = assertion.SelectSingleNode("/samlp:Response/ds:Signature", SamlNamespaceManager);
                var signNode = GetSignatureXmlNode();


                // load the XML signature
                var signedXml = new SignedXml(assertion.DocumentElement);
                signedXml.LoadXml(signNode as XmlElement);

                // get the certificate, basically:
                signedXml.KeyInfo.OfType<KeyInfoX509Data>().First().Certificates.OfType<X509Certificate2>().First();

                bValid = signedXml.CheckSignature(certificate, true);


            }
            catch (Exception)
            {
                throw;
            }

            return bValid;
        } 

        #endregion

        #region CheckExpiryCondition
        public bool CheckExpiryCondition()
        {
            XmlDocument docAssertion = GetAssertionXmlDocument();

            // Check Expiry from 'Conditions' attributes "NotBefore" and "NotOnOrAfter"
            try
            {
                XmlElement el = (XmlElement)docAssertion.GetElementsByTagName("saml:Conditions")[0];
                if (el == null)
                {
                    throw new Exception("SAML Assertion does not contain 'Conditions' tag");
                }
                string strNotBefore = el.Attributes["NotBefore"].Value;
                string strNotOnOrAfter = el.Attributes["NotOnOrAfter"].Value;
                if (strNotBefore.Length < 12 || !Char.IsDigit(strNotBefore[0]) || strNotOnOrAfter.Length < 12 || !Char.IsDigit(strNotOnOrAfter[0]))
                {
                    throw new Exception("Invalid 'NotBefore' or 'NotOnOrAfter' attribute value in 'Conditions' tag");
                }
                DateTime dtNotBefore, dtNotOnOrAfter;
                try
                {
                    dtNotBefore = DateTime.Parse(strNotBefore).ToUniversalTime();
                    dtNotOnOrAfter = DateTime.Parse(strNotOnOrAfter).ToUniversalTime();
                }
                catch (Exception ex)
                {
                    throw new Exception("Invalid 'NotBefore' or 'NotOnOrAfter' date in 'Conditions' tag: " + ex.Message);
                }
                dtNotBefore = dtNotBefore.AddMinutes(-3);
                dtNotOnOrAfter = dtNotOnOrAfter.AddMinutes(3);
                DateTime dtNow = DateTime.Now.ToUniversalTime();
                //DateTime dtNow = DateTime.Now;

                if (dtNow < dtNotBefore || dtNow >= dtNotOnOrAfter)
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        #endregion

        #region GetUserInformation
        public NameValueCollection GetUserInformation()
        {
            NameValueCollection userInfo = new NameValueCollection();
            XmlDocument docAssertion = GetAssertionXmlDocument();

            try
            {
                foreach (XmlElement el in docAssertion.GetElementsByTagName("saml:Attribute"))
                {
                    XmlAttribute attr = el.Attributes["Name"];
                    if (attr != null)
                    {
                        string strAttrName = attr.Value.ToUpper();
                        userInfo[strAttrName] = el.InnerText;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return userInfo;
        } 

        #endregion

        public SamlUserModel GetSamlUser()
        {
            NameValueCollection userInfo = GetUserInformation();

            SamlUserModel model = new SamlUserModel();

            model.UId = userInfo["UID"].ToString();
            model.DisplayName = userInfo["NAME_DISPLAY"].ToString();
            model.FirstName = userInfo["NAME_FIRST"].ToString();
            model.FirstNamePerferred = userInfo["NAME_FIRST_PREFERRED"].ToString();
            model.LastName = userInfo["NAME_LAST"].ToString();
            model.SchoolId = userInfo["SCHOOL_NID"].ToString();
            model.SchoolTitle = userInfo["SCHOOL_TITLE"].ToString();
            model.BuildingId = userInfo["BUILDING_NID"].ToString();
            model.BuildingTitle = userInfo["BUILDING_TITLE"].ToString(); 
            model.IsAdmin = userInfo["IS_ADMIN"].ToString();
            model.RoleId = userInfo["ROLE_ID"].ToString();
            model.RoleName = userInfo["ROLE_NAME"].ToString();
            model.TimeZoneName = userInfo["TIMEZONE_NAME"].ToString();
            model.Domain = userInfo["DOMAIN"].ToString();

            return model;

        }

        public List<Claim> GetUserClaims()
        {
            // Production
            SamlUserModel model = GetSamlUser();
            var claims = new List<Claim>();

            // Display Name
            claims.Add(new Claim("http://www.schoology.com/namedisplay", model.DisplayName));

            // Schoology Unique Id
            claims.Add(new Claim("http://www.schoology.com/uid", model.UId));

            // Name
            claims.Add(new Claim(ClaimTypes.Name, model.FirstName));
            claims.Add(new Claim(ClaimTypes.Surname, model.LastName));

            // Email
            claims.Add(new Claim(ClaimTypes.Email, model.FirstName + "." + model.LastName + "@valorchistian.com"));

            // Admin and Role Id
            claims.Add(new Claim("http://www.schoology.com/isadmin", model.IsAdmin));
            claims.Add(new Claim("http://www.schoology.com/roleid", model.RoleId));

            // Role
            // Get the list of roles.
            List<Role> list = UserService.Instance.GetRoles(); // We will use this later probably, but for now just let it be.

            // If the role has Admin, then let them have access to everything.
            // Note: Currently only Admin and School Admin Authorization is set on the routes.
            string roleName = model.RoleName.ToLower();
            if (roleName.Contains("admin"))
            {
                claims.Add(new Claim(ClaimTypes.Role, "Admin"));


                // Debugging
                //claims.Add(new Claim(ClaimTypes.Role, "Student"));
            }
            else
            {
                claims.Add(new Claim(ClaimTypes.Role, model.RoleName));
            }

            return claims;

        }

        //public List<Claim> GetUserClaims2()
        //{
        //    // Production
        //    NameValueCollection userInfo = GetUserInformation();
        //    var claims = new List<Claim>();

        //    // Display Name
        //    claims.Add(new Claim("http://www.schoology.com/namedisplay", userInfo["NAME_DISPLAY"].ToString()));

        //    // Schoology Unique Id
        //    claims.Add(new Claim("http://www.schoology.com/uid", userInfo["UID"].ToString()));

        //    // Name
        //    claims.Add(new Claim(ClaimTypes.Name, userInfo["NAME_FIRST"].ToString()));
        //    claims.Add(new Claim(ClaimTypes.Surname, userInfo["NAME_LAST"].ToString()));

        //    // Email
        //    claims.Add(new Claim(ClaimTypes.Email, userInfo["NAME_FIRST"].ToString() + "." + userInfo["NAME_LAST"].ToString() + "@valorchistian.com"));

        //    // Admin and Role Id
        //    claims.Add(new Claim("http://www.schoology.com/isadmin", userInfo["IS_ADMIN"]));
        //    claims.Add(new Claim("http://www.schoology.com/roleid", userInfo["ROLE_ID"]));

        //    // Role
        //    // Get the list of roles.
        //    List<Role> list = UserService.Instance.GetRoles();

        //    string roleName = userInfo["ROLE_NAME"].ToString().ToLower();
        //    if (roleName.Contains("admin"))
        //    {
        //        claims.Add(new Claim(ClaimTypes.Role, userInfo["ROLE_NAME"].ToString()));
        //    }
        //    else
        //    {
        //        claims.Add(new Claim(ClaimTypes.Role, userInfo["ROLE_NAME"].ToString()));
        //    }

        //    return claims;

        //}

    }

}
